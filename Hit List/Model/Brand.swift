//
//  Brand.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct Brand: Codable {
    let logo: String
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case logo = "logo_url"
        case name
    }
}
