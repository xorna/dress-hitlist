//
//  Dress.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct Dress: Codable {
    let id: String
    let brand: Brand
    let color: String
    let name: String
    let price: Double
    let season: String
    let images: [String]
}
