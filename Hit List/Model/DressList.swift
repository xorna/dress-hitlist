//
//  DressList.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct DressList: Codable, Equatable {
    let items: [DressListItem]
    let pageCount: Int
    
    enum CodingKeys: String, CodingKey {
        case pageCount = "total_pages"
        case items
    }
}

func +(lhs: DressList?, rhs: DressList?) -> DressList {
    let leftList = lhs?.items ?? [DressListItem]()
    let rightList = rhs?.items ?? [DressListItem]()
    let pageCount = rhs?.pageCount ?? (lhs?.pageCount ?? 1)
    return DressList(items: leftList + rightList, pageCount: pageCount)
}
