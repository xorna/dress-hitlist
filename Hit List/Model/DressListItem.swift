//
//  DressListItem.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct DressListItem: Codable, Equatable {
    let id: String
    let brandName: String
    let name: String
    let price: Double
    let thumbnails: [String]
    
    enum CodingKeys: String, CodingKey {
        case brandName = "brand_name"
        case id
        case name
        case price
        case thumbnails
    }
}
