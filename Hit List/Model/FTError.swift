//
//  FTError.swift
//  Hit List
//
//  Created by Andre Gustavo on 26/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct FTError: Error, Codable, Equatable {
    let localizedDescription: String
    
    enum CodingKeys: CodingKey, String {
        case localizedDescription = "message"
    }
}
