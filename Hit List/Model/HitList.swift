//
//  HitList.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct HitList: Codable, Equatable {
    let rating: Int
    let lines: [Line]
    let total: Int
    
    enum CodingKeys: String, CodingKey {
        case rating = "average_rating"
        case lines
        case total
    }
}

extension HitList {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        lines = try container.decode([Line].self, forKey: .lines)
        total = try container.decode(Int.self, forKey: .total)
        do {
            rating = try container.decode(Int.self, forKey: .rating)
        } catch {
            rating = 0
        }
    }
}
