//
//  Line.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct Line: Codable, Equatable {
    var id: String?
    let dressId: String
    let rating: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "line_id"
        case dressId = "dress_id"
        case rating
    }
}
