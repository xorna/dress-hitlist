//
//  DressDetailsBuilder.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

protocol DressDetailsBuilder {
    func buildDressDetailsModule(dress id: String) -> UIViewController?
}
