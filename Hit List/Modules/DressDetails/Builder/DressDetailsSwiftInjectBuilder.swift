//
//  DressDetailsSwiftInjectBuilder.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit
import Swinject

class DressDetailsSwiftInjectBuilder {
    
    private let container: Container
    
    required init(parent: Container) {
        container = Container(parent: parent)
    }
    
    private func registerView() {
        container.register(DressDetailsView.self, factory: { _ in
            DressDetailsViewController(collectionViewLayout: UICollectionViewFlowLayout())
        }).initCompleted({ r, view in
            if let view = view as? DressDetailsViewController {
                view.presenter = r.resolve(DressDetailsPresenter.self)!
            }
        }).inObjectScope(.container)
    }
    
    private func registerInteractor(dressId: String) {
        container.register(DressDetailsInteractor.self, factory: { r in
            DressDetailsDefaultInteractor(dressService: r.resolve(DressesService.self)!,
                                          similarService: r.resolve(SimilarService.self)!,
                                          hitListService: r.resolve(HitListService.self)!,
                                          dress: dressId)
        }).initCompleted({ (r, interactor) in
            if let interactor = interactor as? DressDetailsDefaultInteractor {
                interactor.presenter = r.resolve(DressDetailsPresenter.self)
            }
        }).inObjectScope(.container)
    }
    
    private func registerRouter() {
        container.register(DressDetailsRouter.self, factory: { _ in
            DressDetailsDefaultRouter()
        }).initCompleted({ r, router in
            if let router = router as? DressDetailsDefaultRouter {
                router.viewController = r.resolve(DressDetailsView.self) as? UIViewController
                router.dressDetailsBuilder = { dressId in
                    let builder = r.resolve(DressDetailsBuilder.self)
                    return builder?.buildDressDetailsModule(dress: dressId)
                }
                router.hitListModalBuilder = { dress, line in
                    let builder = r.resolve(HitListModalBuilder.self)
                    return builder?.buildModule(dress: dress, line: line)
                }
            }
        })
    }
    
    private func registerPresenter() {
        container.register(DressDetailsPresenter.self, factory: { r in
            DressDetailsDefaultPresenter(interactor: r.resolve(DressDetailsInteractor.self)!,
                                         router: r.resolve(DressDetailsRouter.self)!,
                                         priceFormatter: r.resolve(NumberFormatter.self, name: "currency_formatter")!)
        }).initCompleted({ r, presenter in
            if let presenter = presenter as? DressDetailsDefaultPresenter {
                presenter.view = r.resolve(DressDetailsView.self)
            }
        }).inObjectScope(.container)
    }
}

extension DressDetailsSwiftInjectBuilder: DressDetailsBuilder {
    func buildDressDetailsModule(dress id: String) -> UIViewController? {
        registerView()
        registerInteractor(dressId: id)
        registerRouter()
        registerPresenter()
        
        return container.resolve(DressDetailsView.self) as? UIViewController
    }
}
