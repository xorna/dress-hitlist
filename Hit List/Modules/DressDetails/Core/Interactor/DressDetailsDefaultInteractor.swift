//
//  DressDetailsDefaultInteractor.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

class DressDetailsDefaultInteractor {
    
    private let dressService: DressesService
    private let similarService: SimilarService
    private let hitListService: HitListService
    
    let dressId: String
    
    var details: Dress?
    var line: Line?
    
    weak var presenter: DressDetailsPresenter?
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .didSaveItemToHitList, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didDeleteItemFromHitList, object: nil)
    }
    
    required init(dressService: DressesService, similarService: SimilarService, hitListService: HitListService, dress id: String) {
        self.dressService = dressService
        self.similarService = similarService
        self.hitListService = hitListService
        self.dressId = id
    }
    
    private func loadSimilarDresses() {
        loadSimilarAppearance()
        loadSimilarColor()
        loadSimilarShape()
        loadSimilarTexture()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSaveToHitList(notification:)), name: .didSaveItemToHitList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didDeleteFromHitList(notification:)), name: .didDeleteItemFromHitList, object: nil)
    }
    
    @objc func didSaveToHitList(notification: Notification) {
        guard let savedLine = notification.object as? Line, let dress = details, savedLine.dressId == dressId else {
            return
        }
        line = savedLine
        presenter?.presentDressDetails(dress, line: savedLine)
    }
    
    @objc func didDeleteFromHitList(notification: Notification) {
        guard let deletedLine = notification.object as? String, let dress = details, deletedLine == line?.id else {
            return
        }
        line = nil
        presenter?.presentDressDetails(dress, line: nil)
    }
    
    private func loadSimilarAppearance() {
        similarService.find(id: dressId, by: .appearance) { [presenter] (list, error) in
            guard let list = list else {
                return
            }
            
            presenter?.presentSimilar(list, with: .appearance)
        }
    }
    
    private func loadSimilarColor() {
        similarService.find(id: dressId, by: .color) { [presenter] (list, error) in
            guard let list = list else {
                return
            }
            
            presenter?.presentSimilar(list, with: .color)
        }
    }
    
    private func loadSimilarShape() {
        similarService.find(id: dressId, by: .shape) { [presenter] (list, error) in
            guard let list = list else {
                return
            }
            
            presenter?.presentSimilar(list, with: .shape)
        }
    }
    
    private func loadSimilarTexture() {
        similarService.find(id: dressId, by: .texture) { [presenter] (list, error) in
            guard let list = list else {
                return
            }
            
            presenter?.presentSimilar(list, with: .texture)
        }
    }
    
    private func updatePresenter() {
        guard let details = details else {
            return
        }
        presenter?.presentDressDetails(details, line: line)
    }
}

extension DressDetailsDefaultInteractor: DressDetailsInteractor {
    func loadDressDetails() {
        dressService.get(dress: dressId) { [weak self] (dress, error) in
            guard let dress = dress else {
                self?.presenter?.presentError(error?.localizedDescription ?? "Unknown error")
                return
            }
            
            self?.loadSimilarDresses()
            self?.details = dress
            
            self?.hitListService.getHitlist({ [weak self] (list, error) in
                guard let list = list else {
                    self?.presenter?.presentError(error?.localizedDescription ?? "Unknown error")
                    return
                }
                self?.line = list.lines.first(where: {$0.dressId == dress.id})
                self?.updatePresenter()
            })
        }
    }
    
    func addToHitList(rating: Int) {
        let line = Line(id: nil, dressId: dressId, rating: rating)
        hitListService.save(line: line) { [weak self] (line, error) in
            self?.line = line
            self?.updatePresenter()
        }
    }
    
    func updateHitList(rating: Int) {
        guard let line = line, let lineId = line.id else {
            return
        }
        self.line = Line(id: lineId, dressId: dressId, rating: rating)
        hitListService.update(line: self.line!) { [weak self] (line, error) in
            guard let line = line else {
                return
            }
            self?.line = line
            self?.updatePresenter()
        }
    }
    
    func removeFromHitList() {
        guard let line = line else {
            updatePresenter()
            return
        }
        
        guard let lineId = line.id else {
            self.line = nil
            updatePresenter()
            return
        }
        
        hitListService.delete(line: lineId) { [weak self] (error) in
            if let error = error {
                self?.presenter?.presentError(error.localizedDescription)
                return
            }
            self?.line = nil
            self?.updatePresenter()
        }
    }
}
