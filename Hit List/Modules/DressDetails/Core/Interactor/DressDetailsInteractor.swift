//
//  DressDetailsInteractor.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol DressDetailsInteractor {
    var dressId: String { get }
    
    var details: Dress? { get }
    var line: Line? { get }
    
    func loadDressDetails()
    func addToHitList(rating: Int)
    func updateHitList(rating: Int)
    func removeFromHitList()
}
