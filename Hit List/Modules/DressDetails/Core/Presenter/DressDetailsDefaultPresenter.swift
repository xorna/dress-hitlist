//
//  DressDetailsDefaultPresenter.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation
import PKHUD
import PopupDialog

class DressDetailsDefaultPresenter {
    
    private let interactor: DressDetailsInteractor
    private let router: DressDetailsRouter
    private let priceFormatter: NumberFormatter
    
    weak var view: DressDetailsView?
    
    required init(interactor: DressDetailsInteractor, router: DressDetailsRouter, priceFormatter: NumberFormatter) {
        self.interactor = interactor
        self.router = router
        self.priceFormatter = priceFormatter
    }
    
}

extension DressDetailsDefaultPresenter: DressDetailsPresenter {
    func loadContent() {
        PKHUD.sharedHUD.contentView = PKHUDSystemActivityIndicatorView()
        PKHUD.sharedHUD.show()
        
        // Preload similar titles
        view?.updateSimilarAppearanceViewModel(DressDetailsSimilarViewModel(title: "Similar Appearance", dresses: []))
        view?.updateSimilarColorViewModel(DressDetailsSimilarViewModel(title: "Similar Color", dresses: []))
        view?.updateSimilarShapeViewModel(DressDetailsSimilarViewModel(title: "Similar Shape", dresses: []))
        view?.updateSimilarTextureViewModel(DressDetailsSimilarViewModel(title: "Similar Texture", dresses: []))
        
        interactor.loadDressDetails()
    }
    
    func presentDressDetails(_ details: Dress, line: Line?) {
        let brandViewModel = DressDetailsBrandViewModel(logoURL: URL(string: details.brand.logo)!, name: details.brand.name)
        let imageURLs = details.images.compactMap({URL(string: $0)})
        
        let viewModel = DressDetailsViewModel(id: details.id,
                                              brand: brandViewModel,
                                              color: details.color,
                                              name: details.name,
                                              price: priceFormatter.string(from: NSNumber(value: details.price)) ?? "€ -,--",
                                              season: details.season,
                                              images: imageURLs,
                                              isHitListed: line != nil,
                                              rating: line?.rating ?? 0)

        DispatchQueue.main.async { [view] in
            PKHUD.sharedHUD.hide()
            view?.updateVieModel(viewModel)
        }
    }
    
    func presentHitListPopUp() {
        guard let dress = interactor.details else {
            return
        }
        
        router.showHitListPopUp(dress: dress, line: interactor.line)
    }
    
    func presentSimilar(_ data: DressList, with similarity: Similarity) {
        
        let list = data.items.map({ [priceFormatter] in
            DressViewModel(id: $0.id,
                           name: $0.name,
                           brand: $0.brandName,
                           price: priceFormatter.string(from: NSNumber(value: $0.price)) ?? "€ -,--",
                           thumbnailURL: URL(string: $0.thumbnails[0]),
                           isHitListed: false)
        })
        
        DispatchQueue.main.async { [view, router] in
            switch similarity {
            case .appearance:
                let viewModel = DressDetailsSimilarViewModel(title: "Similar Appearance", dresses: list)
                viewModel.selectionBlock = { [router] id in
                    router.navigateToDressDetails(dress: id)
                }
                view?.updateSimilarAppearanceViewModel(viewModel)
                break
            case .color:
                let viewModel = DressDetailsSimilarViewModel(title: "Similar Color", dresses: list)
                viewModel.selectionBlock = { [router] id in
                    router.navigateToDressDetails(dress: id)
                }
                view?.updateSimilarColorViewModel(viewModel)
                break
            case .shape:
                let viewModel = DressDetailsSimilarViewModel(title: "Similar Shape", dresses: list)
                viewModel.selectionBlock = { [router] id in
                    router.navigateToDressDetails(dress: id)
                }
                view?.updateSimilarShapeViewModel(viewModel)
                break
            case .texture:
                let viewModel = DressDetailsSimilarViewModel(title: "Similar Texture", dresses: list)
                viewModel.selectionBlock = { [router] id in
                    router.navigateToDressDetails(dress: id)
                }
                view?.updateSimilarTextureViewModel(viewModel)
                break
            }
        }
    }
    
    func presentError(_ message: String) {
        DispatchQueue.main.async {
            PKHUD.sharedHUD.contentView = PKHUDErrorView(title: "Error", subtitle: message)
            PKHUD.sharedHUD.hide(afterDelay: 2.0)
        }
    }
}
