//
//  DressDetailsPresenter.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol DressDetailsPresenter: class {
    func loadContent()
    func presentDressDetails(_ details: Dress, line: Line?)
    func presentSimilar(_ data: DressList, with similarity: Similarity)
    func presentHitListPopUp()
    func presentError(_ message: String)
}
