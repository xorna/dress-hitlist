//
//  DressDetailSimilarDressCollectionViewCell.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class DressDetailSimilarDressCollectionViewCell: UICollectionViewCell {
    
    static let similarDressCellId: String = "DRESS_DETAILS_SIMILAR_DRESS_CELL_ID"
    
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var priceLabel: UILabel?

    func configure(viewModel: DressViewModel) {
        imageView?.image = #imageLiteral(resourceName: "imagePlaceholder")
        if let thumbURL = viewModel.thumbnailURL {
            imageView?.af_setImage(withURL: thumbURL)
        }
        
        nameLabel?.text = viewModel.name
        priceLabel?.text = viewModel.price
    }
}
