//
//  DressDetailsBrandCollectionViewCell.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class DressDetailsBrandCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var nameLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView?.layer.borderWidth = 0.5
        imageView?.layer.borderColor = UIColor(red: 0xD0/255.0, green: 0x02/255.0, blue: 0x1B/255.0, alpha: 0.8).cgColor
    }

    func configure(imageURL: URL, dressName: String) {
        imageView?.af_setImage(withURL: imageURL)
        nameLabel?.text = dressName
    }

}
