//
//  DressDetailsCharacteristicCollectionViewCell.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class DressDetailsCharacteristicCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var valueLabel: UILabel?

    func configure(title: String, value: String) {
        titleLabel?.text = title
        valueLabel?.text = value
    }
}
