//
//  DressDetailsGalleryCollectionViewCell.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit
import ImageSlideshow

class DressDetailsGalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var slideShow: ImageSlideshow?
    weak var viewController: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapImage))
        slideShow?.addGestureRecognizer(gestureRecognizer)
    }

    func configure(images: [URL]) {
        slideShow?.setImageInputs(images.map({AlamofireSource(url: $0)}))
    }
    
    @objc func didTapImage() {
        if let viewController = viewController {
            slideShow?.presentFullScreenController(from: viewController)
        }
    }
}
