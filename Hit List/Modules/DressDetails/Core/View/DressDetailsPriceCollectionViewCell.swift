//
//  DressDetailsPriceCollectionViewCell.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class DressDetailsPriceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var priceLabel: UILabel?
    @IBOutlet weak var hitlistButton: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        hitlistButton?.imageView?.contentMode = .scaleAspectFit
    }

    func configure(price: String, rating: Int) {
        priceLabel?.text = price
        guard rating < 6 else {
            hitlistButton?.setImage(#imageLiteral(resourceName: "Rating_5"), for: .normal)
            return
        }
        let images = [
            #imageLiteral(resourceName: "Rating_0"), #imageLiteral(resourceName: "Rating_1"), #imageLiteral(resourceName: "Rating_2"), #imageLiteral(resourceName: "Rating_3"), #imageLiteral(resourceName: "Rating_4"), #imageLiteral(resourceName: "Rating_5")
        ]
        hitlistButton?.setImage(images[rating], for: .normal)
    }
}
