//
//  DressDetailsSimilarCollectionViewCell.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class DressDetailsSimilarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var collectionView: UICollectionView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView?.register(UINib(nibName: "DressDetailSimilarDressCollectionViewCell", bundle: nil),
                                 forCellWithReuseIdentifier: DressDetailSimilarDressCollectionViewCell.similarDressCellId)
    }

    func configure(viewModel: DressDetailsSimilarViewModel) {
        titleLabel?.text = viewModel.title
        collectionView?.dataSource = viewModel
        collectionView?.delegate = viewModel
    }
}
