//
//  DressDetailsView.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol DressDetailsView: class {
    func updateVieModel(_ viewModel: DressDetailsViewModel)
    func updateSimilarAppearanceViewModel(_ viewModel: DressDetailsSimilarViewModel)
    func updateSimilarColorViewModel(_ viewModel: DressDetailsSimilarViewModel)
    func updateSimilarShapeViewModel(_ viewModel: DressDetailsSimilarViewModel)
    func updateSimilarTextureViewModel(_ viewModel: DressDetailsSimilarViewModel)
}
