//
//  DressDetailsViewController.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class DressDetailsViewController: UICollectionViewController {
    
    private let galleryCellId = "DRESS_DETAILS_GALLERY_CELL_ID"
    private let brandCellId = "DRESS_DETAILS_BRAND_CELL_ID"
    private let priceCellId = "DRESS_DETAILS_PRICE_CELL_ID"
    private let characteristicCellId = "DRESS_DETAILS_CHARACTERISTIC_CELL_ID"
    private let similarCellId = "DRESS_DETAILS_SIMILAR_CELL_ID"
    
    var presenter: DressDetailsPresenter!
    
    private var viewModel: DressDetailsViewModel? {
        didSet {
            title = viewModel?.brand.name
            collectionView?.reloadData()
        }
    }
    
    private var similarAppearanceViewModel: DressDetailsSimilarViewModel? {
        didSet {
            if collectionView!.numberOfSections >= 5 {
                collectionView?.reloadSections(IndexSet(integer: 5))
            }
        }
    }
    
    private var similarColorViewModel: DressDetailsSimilarViewModel? {
        didSet {
            if collectionView!.numberOfSections >= 6 {
                collectionView?.reloadSections(IndexSet(integer: 6))
            }
        }
    }
    
    private var similarShapeViewModel: DressDetailsSimilarViewModel? {
        didSet {
            if collectionView!.numberOfSections >= 7 {
                collectionView?.reloadSections(IndexSet(integer: 7))
            }
        }
    }
    
    private var similarTextureViewModel: DressDetailsSimilarViewModel? {
        didSet {
            if collectionView!.numberOfSections >= 8 {
                collectionView?.reloadSections(IndexSet(integer: 8))
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = .white
        
        collectionView?.register(UINib(nibName: "DressDetailsGalleryCollectionViewCell", bundle: nil),
                                 forCellWithReuseIdentifier: galleryCellId)
        collectionView?.register(UINib(nibName: "DressDetailsBrandCollectionViewCell", bundle: nil),
                                 forCellWithReuseIdentifier: brandCellId)
        collectionView?.register(UINib(nibName: "DressDetailsPriceCollectionViewCell", bundle: nil),
                                 forCellWithReuseIdentifier: priceCellId)
        collectionView?.register(UINib(nibName: "DressDetailsCharacteristicCollectionViewCell", bundle: nil),
                                 forCellWithReuseIdentifier: characteristicCellId)
        collectionView?.register(UINib(nibName: "DressDetailsSimilarCollectionViewCell", bundle: nil),
                                 forCellWithReuseIdentifier: similarCellId)
        
        presenter.loadContent()
    }

    @objc func tappedHitListButton() {
        presenter.presentHitListPopUp()
    }
}

extension DressDetailsViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard viewModel != nil else {
            return 0
        }
        
        return 9
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            return buildAndConfigureGalleryCell(collectionView: collectionView, indexPath: indexPath)
        case 1:
            return buildAndConfigureBrandCell(collectionView: collectionView, indexPath: indexPath)
        case 2:
            return buildAndConfigurePriceCell(collectionView: collectionView, indexPath: indexPath)
        case 3:
            return buildAndConfigureCharacteristicCell(collectionView: collectionView, indexPath: indexPath, title: "Color:", value: viewModel?.color ?? "-")
        case 4:
            return buildAndConfigureCharacteristicCell(collectionView: collectionView, indexPath: indexPath, title: "Season:", value: viewModel?.season ?? "-")
        case 5:
            return buildAndConfigureSimilarCell(collectionView: collectionView, indexPath: indexPath, viewModel: similarAppearanceViewModel)
        case 6:
            return buildAndConfigureSimilarCell(collectionView: collectionView, indexPath: indexPath, viewModel: similarColorViewModel)
        case 7:
            return buildAndConfigureSimilarCell(collectionView: collectionView, indexPath: indexPath, viewModel: similarShapeViewModel)
        case 8:
            return buildAndConfigureSimilarCell(collectionView: collectionView, indexPath: indexPath, viewModel: similarTextureViewModel)
        default:
            return UICollectionViewCell()
        }
    }
    
    private func buildAndConfigureGalleryCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: galleryCellId, for: indexPath)
        if let galleryCell = cell as? DressDetailsGalleryCollectionViewCell, let viewModel = viewModel {
            galleryCell.configure(images: viewModel.images)
            galleryCell.viewController = self
        }
        return cell
    }
    
    private func buildAndConfigureBrandCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: brandCellId, for: indexPath)
        if let brandCell = cell as? DressDetailsBrandCollectionViewCell, let viewModel = viewModel {
            brandCell.configure(imageURL: viewModel.brand.logoURL, dressName: viewModel.name)
        }
        return cell
    }
    
    private func buildAndConfigurePriceCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: priceCellId, for: indexPath)
        if let priceCell = cell as? DressDetailsPriceCollectionViewCell, let viewModel = viewModel {
            priceCell.configure(price: viewModel.price, rating: viewModel.rating)
            priceCell.hitlistButton?.removeTarget(self, action: #selector(tappedHitListButton), for: .touchUpInside)
            priceCell.hitlistButton?.addTarget(self, action: #selector(tappedHitListButton), for: .touchUpInside)
        }
        return cell
    }
    
    private func buildAndConfigureCharacteristicCell(collectionView: UICollectionView, indexPath: IndexPath, title: String, value: String) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: characteristicCellId, for: indexPath)
        if let characteristicCell = cell as? DressDetailsCharacteristicCollectionViewCell {
            characteristicCell.configure(title: title, value: value)
        }
        return cell
    }
    
    private func buildAndConfigureSimilarCell(collectionView: UICollectionView, indexPath: IndexPath, viewModel: DressDetailsSimilarViewModel?) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: similarCellId, for: indexPath)
        if let similarCell = cell as? DressDetailsSimilarCollectionViewCell, let viewModel = viewModel {
            similarCell.configure(viewModel: viewModel)
        }
        return cell
    }
}

extension DressDetailsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.width * 0.75)
        case 1:
            return CGSize(width: collectionView.frame.width, height: 80.0)
        case 2:
            return CGSize(width: collectionView.frame.width, height: 44.0)
        case 3:
            return CGSize(width: collectionView.frame.width, height: 30.0)
        case 4:
            return CGSize(width: collectionView.frame.width, height: 30.0)
        default:
            return CGSize(width: collectionView.frame.width, height: 224.0)
        }
    }
}

extension DressDetailsViewController: DressDetailsView {
    func updateVieModel(_ viewModel: DressDetailsViewModel) {
        self.viewModel = viewModel
    }
    
    func updateSimilarAppearanceViewModel(_ viewModel: DressDetailsSimilarViewModel) {
        similarAppearanceViewModel = viewModel
    }
    
    func updateSimilarColorViewModel(_ viewModel: DressDetailsSimilarViewModel) {
        similarColorViewModel = viewModel
    }
    
    func updateSimilarShapeViewModel(_ viewModel: DressDetailsSimilarViewModel) {
        similarShapeViewModel = viewModel
    }
    
    func updateSimilarTextureViewModel(_ viewModel: DressDetailsSimilarViewModel) {
        similarTextureViewModel = viewModel
    }
}
