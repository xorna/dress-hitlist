//
//  DressDetailsBrandViewModel.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct DressDetailsBrandViewModel {
    let logoURL: URL
    let name: String
}
