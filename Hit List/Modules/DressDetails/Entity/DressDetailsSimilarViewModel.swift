//
//  DressDetailsSimilarViewModel.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class DressDetailsSimilarViewModel: NSObject {
    let title: String
    let dresses: [DressViewModel]
    
    var selectionBlock: ((String) -> Void)?
    
    required init(title: String, dresses: [DressViewModel]) {
        self.title = title
        self.dresses = dresses
    }
}

extension DressDetailsSimilarViewModel: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dresses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DressDetailSimilarDressCollectionViewCell.similarDressCellId, for: indexPath)
        if let dressCell = cell as? DressDetailSimilarDressCollectionViewCell, indexPath.item < dresses.count {
            dressCell.configure(viewModel: dresses[indexPath.item])
        }
        return cell
    }
}

extension DressDetailsSimilarViewModel: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.item < dresses.count else { return }
        selectionBlock?(dresses[indexPath.item].id)
    }
}
