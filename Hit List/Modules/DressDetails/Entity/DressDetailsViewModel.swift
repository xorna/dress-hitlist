//
//  DressDetailsViewModel.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct DressDetailsViewModel {
    let id: String
    let brand: DressDetailsBrandViewModel
    let color: String
    let name: String
    let price: String
    let season: String
    let images: [URL]
    let isHitListed: Bool
    let rating: Int
}
