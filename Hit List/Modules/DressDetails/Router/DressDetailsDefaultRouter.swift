//
//  DressDetailsDefaultRouter.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit
import PopupDialog

class DressDetailsDefaultRouter {
    weak var viewController: UIViewController?
    
    var dressDetailsBuilder: ((String) -> UIViewController?)?
    var hitListModalBuilder: ((Dress, Line?) -> UIViewController?)?
}

extension DressDetailsDefaultRouter: DressDetailsRouter {
    func navigateToDressDetails(dress id: String) {
        guard let module = dressDetailsBuilder?(id) else {
            return
        }
        
        viewController?.navigationController?.pushViewController(module, animated: true)
    }
    
    func showHitListPopUp(dress: Dress, line: Line?) {
        guard let modalViewController = hitListModalBuilder?(dress, line) else {
            return
        }
        
        let popup = PopupDialog(viewController: modalViewController)
        
        if let modalViewController = modalViewController as? HitListModalViewController {
            if line?.id != nil {
                popup.addButton(DestructiveButton(title: "Remove from hit list", action: {
                    modalViewController.deleteAction()
                }))
            }
            
            popup.addButton(DefaultButton(title: "Save", action: {
                modalViewController.saveAction()
            }))
        }
        
        popup.addButton(CancelButton(title: "Cancel", action: { [viewController] in
            viewController?.dismiss(animated: true, completion: nil)
        }))
        
        viewController?.present(popup, animated: true, completion: nil)
    }
}
