//
//  DressDetailsRouter.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol DressDetailsRouter {
    func navigateToDressDetails(dress id: String)
    func showHitListPopUp(dress: Dress, line: Line?)
}
