//
//  DressListBuilder.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

protocol DressListBuilder {
    func buildDressListModule() -> UIViewController?
}
