//
//  DressListSwiftInjectBuilder.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit
import Swinject

class DressListSwiftInjectBuilder {
    
    private let container: Container
    
    required init(parent: Container) {
        container = Container(parent: parent)
    }
}

extension DressListSwiftInjectBuilder: DressListBuilder {
    func buildDressListModule() -> UIViewController? {
        registerView()
        registerInteractor()
        registerRouter()
        registerPresenter()
        
        return container.resolve(DressListView.self) as? UIViewController
    }
    
    private func registerView() {
        container.register(DressListView.self, factory: { _ in
            DressListViewController(collectionViewLayout: UICollectionViewFlowLayout())
        }).initCompleted({ (r, view) in
            if let view = view as? DressListViewController {
                view.presenter = r.resolve(DressListPresenter.self)
            }
        }).inObjectScope(.container)
    }
    
    private func registerInteractor() {
        container.register(DressListInteractor.self, factory: { r in
            DressListDefaultInteractor(dressesService: r.resolve(DressesService.self)!,
                                       hitListService: r.resolve(HitListService.self)!)
        }).initCompleted({ (r, interactor) in
            if let interactor = interactor as? DressListDefaultInteractor {
                interactor.presenter = r.resolve(DressListPresenter.self)
            }
        }).inObjectScope(.container)
    }
    
    private func registerPresenter() {
        container.register(DressListPresenter.self, factory: { r in
            DressListDefaultPresenter(interactor: r.resolve(DressListInteractor.self)!,
                                      router: r.resolve(DressListRouter.self)!,
                                      currencyFormatter: r.resolve(NumberFormatter.self, name: "currency_formatter")!)
        }).initCompleted({ (r, presenter) in
            if let presenter = presenter as? DressListDefaultPresenter {
                presenter.view = r.resolve(DressListView.self)
            }
        }).inObjectScope(.container)
    }
    
    private func registerRouter() {
        container.register(DressListRouter.self, factory: { _ in
            DressListDefaultRouter()
        }).initCompleted({ (r, router) in
            if let router = router as? DressListDefaultRouter {
                router.viewController = r.resolve(DressListView.self) as? UIViewController
                router.dressDetailsBuilder = { dressId in
                    let builder = r.resolve(DressDetailsBuilder.self)
                    return builder?.buildDressDetailsModule(dress: dressId)
                }
            }
        }).inObjectScope(.container)
    }
}
