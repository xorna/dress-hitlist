//
//  DressListDefaultInteractor.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

class DressListDefaultInteractor {
    
    private let defaultPageSize = 20
    private var dresses: DressList?
    
    private let dressesService: DressesService
    private let hitListService: HitListService
    weak var presenter: DressListPresenter?
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .didSaveItemToHitList, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didDeleteItemFromHitList, object: nil)
    }
    
    required init(dressesService: DressesService, hitListService: HitListService) {
        self.dressesService = dressesService
        self.hitListService = hitListService
        
        NotificationCenter.default.addObserver(self, selector: #selector(itemAddedToHitList(notification:)), name: .didSaveItemToHitList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(itemRemovedFromHitList(notification:)), name: .didDeleteItemFromHitList, object: nil)
    }
    
    @objc func itemAddedToHitList(notification: Notification) {
        if let dressList = dresses {
            handleDresses(dressList, requestedPage: 1)
        }
    }
    
    @objc func itemRemovedFromHitList(notification: Notification) {
        if let dressList = dresses {
            handleDresses(dressList, requestedPage: 1)
        }
    }
}

extension DressListDefaultInteractor: DressListInteractor {
    func loadDresses(page: Int, sortOn: DressesRequestSortField?, sortOrder: DressesRequestSortOrder?) {
        let request = DressesRequest(pageSize: defaultPageSize, pageNum: page, sortOn: sortOn, sortOrder: sortOrder)
        dressesService.getAll(request: request) { [unowned self] (list, error) in
            guard let list = list else {
                return self.handleError(error)
            }
            self.handleDresses(list, requestedPage: page)
        }
    }
    
    private func handleDresses(_ dresses: DressList, requestedPage: Int) {
        if requestedPage == 1 || self.dresses == nil {
            self.dresses = dresses
        } else {
            self.dresses = self.dresses + dresses
        }
        hitListService.getHitlist { [presenter, handleError] (list, error) in
            guard let list = list else {
                handleError(error)
                return
            }
            presenter?.append(dresses: dresses, hitList: list, page: requestedPage)
        }
    }
    
    private func handleError(_ error: FTError?) {
        let requestError = error ?? FTError(localizedDescription: "Unknown error")
        presenter?.presentError(requestError)
    }
}
