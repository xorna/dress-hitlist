//
//  DressListInteractor.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol DressListInteractor {
    func loadDresses(page: Int, sortOn: DressesRequestSortField?, sortOrder: DressesRequestSortOrder?)
}
