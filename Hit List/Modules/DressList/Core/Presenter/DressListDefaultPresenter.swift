//
//  DressListDefaultPresenter.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation
import PKHUD

class DressListDefaultPresenter {
    
    private let interactor: DressListInteractor
    private let router: DressListRouter
    private let currencyFormatter: NumberFormatter
    weak var view: DressListView?
    
    private var currentPage: Int = 1
    private var sortBy: DressesRequestSortField?
    private var sortOrder: DressesRequestSortOrder?
    private var dressListViewModel = DressListViewModel(dresses: [], canPaginate: false, sortOrder: "", sortBy: "")
    
    required init(interactor: DressListInteractor, router: DressListRouter, currencyFormatter: NumberFormatter) {
        self.interactor = interactor
        self.router = router
        self.currencyFormatter = currencyFormatter
    }
    
    private func showLoadingHUD() {
        PKHUD.sharedHUD.contentView = PKHUDSystemActivityIndicatorView()
        PKHUD.sharedHUD.show()
    }
}

extension DressListDefaultPresenter: DressListPresenter {
    func loadContent() {
        currentPage = 1
        showLoadingHUD()
        interactor.loadDresses(page: currentPage, sortOn: sortBy, sortOrder: sortOrder)
    }
    
    func loadNexPage() {
        currentPage += 1
        interactor.loadDresses(page: currentPage, sortOn: sortBy, sortOrder: sortOrder)
    }
    
    func append(dresses: DressList, hitList: HitList, page: Int) {
        var viewModels = page > 1 ? dressListViewModel.dresses : [DressViewModel]()
        
        viewModels.append(contentsOf: dresses.items.map({ [currencyFormatter] item in
            let isHitListed = hitList.lines.contains(where: {$0.dressId == item.id})
            let formattedPrice = currencyFormatter.string(from: NSNumber(value: item.price)) ?? "\(currencyFormatter.currencySymbol) -,--"
            let thumbnailPath = item.thumbnails.first ?? ""
            return DressViewModel(id: item.id,
                                  name: item.name,
                                  brand: item.brandName,
                                  price: formattedPrice,
                                  thumbnailURL: URL(string: thumbnailPath),
                                  isHitListed: isHitListed)
        }))
        
        dressListViewModel = DressListViewModel(dresses: viewModels,
                                                canPaginate: dresses.pageCount > 0,
                                                sortOrder: sortOrder?.rawValue.capitalized ?? "",
                                                sortBy: sortBy?.rawValue.capitalized ?? "")
        
        DispatchQueue.main.async { [view, dressListViewModel] in
            PKHUD.sharedHUD.hide()
            view?.updateList(viewModel: dressListViewModel)
        }
    }
    
    func presentError(_ error: FTError) {
        DispatchQueue.main.async { [view] in
            PKHUD.sharedHUD.hide()
            view?.displayError(message: error.localizedDescription)
        }
    }
    
    func presentDressDetail(dress id: String) {
        router.navigateToDressDetails(dress: id)
    }
    
    func presentSortPicker() {
        router.openSortPicker { [weak self] (field, order) in
            self?.changeSort(by: field, order: order)
        }
    }
    
    func changeSort(by field: DressesRequestSortField?, order: DressesRequestSortOrder?) {
        showLoadingHUD()
        currentPage = 1
        sortBy = field
        sortOrder = order
        interactor.loadDresses(page: currentPage, sortOn: sortBy, sortOrder: sortOrder)
    }
}
