//
//  DressListPresenter.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol DressListPresenter: class {
    func loadContent()
    func loadNexPage()
    func append(dresses: DressList, hitList: HitList, page: Int)
    func presentError(_ error: FTError)
    func presentDressDetail(dress id: String)
    func presentSortPicker()
    func changeSort(by field: DressesRequestSortField?, order: DressesRequestSortOrder?)
}
