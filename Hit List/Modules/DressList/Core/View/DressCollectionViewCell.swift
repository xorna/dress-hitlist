//
//  DressCollectionViewCell.swift
//  Hit List
//
//  Created by Andre Gustavo on 28/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit
import AlamofireImage

class DressCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var brandLabel: UILabel?
    @IBOutlet weak var priceLabel: UILabel?
    @IBOutlet weak var hitListTag: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        hitListTag?.isHidden = true
        contentView.layer.borderWidth = 0.5
        contentView.layer.borderColor = UIColor(red: 0xD0/255.0, green: 0x02/255.0, blue: 0x1B/255.0, alpha: 0.8).cgColor
    }

    func configure(viewModel: DressViewModel) {
        nameLabel?.text = viewModel.name
        brandLabel?.text = viewModel.brand
        priceLabel?.text = viewModel.price
        hitListTag?.isHidden = !viewModel.isHitListed
        
        if let thumbURL = viewModel.thumbnailURL {
            imageView?.af_setImage(withURL: thumbURL)
        }
    }
}
