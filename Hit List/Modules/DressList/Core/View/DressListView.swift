//
//  DressListView.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol DressListView: class {
    func updateList(viewModel: DressListViewModel)
    func displayError(message: String)
}
