//
//  DressListViewController.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit
import PKHUD

class DressListViewController: UICollectionViewController {
    
    private let dressCellId = "DRESS_CELL"
    private let headerViewId = "SORT_HEADER"
    
    var presenter: DressListPresenter?
    
    private var viewModel: DressListViewModel? {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Dresses"
        
        collectionView?.backgroundColor = .white
        collectionView?.register(UINib(nibName: "DressCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: dressCellId)
        collectionView?.register(UINib(nibName: "DressListHeaderCollectionReusableView", bundle: nil),
                                 forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                 withReuseIdentifier: headerViewId)
        
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 5
            layout.minimumInteritemSpacing = 5
            layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            layout.headerReferenceSize = CGSize(width: 0, height: 44)
        }
        
        presenter?.loadContent()
    }
    
    @objc func openSortSelector() {
        presenter?.presentSortPicker()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView?.collectionViewLayout.invalidateLayout()
        collectionView?.reloadData()
    }
}

extension DressListViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.dresses.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: dressCellId, for: indexPath)
        
        if let dressCell = cell as? DressCollectionViewCell, let dress = viewModel?.dresses[indexPath.item] {
            dressCell.imageView?.image = #imageLiteral(resourceName: "imagePlaceholder")
            dressCell.configure(viewModel: dress)
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let viewModel = viewModel, viewModel.canPaginate, indexPath.item == viewModel.dresses.count - 6 else {
            return
        }
        
        presenter?.loadNexPage()
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerViewId, for: indexPath)
        
        if let header = supplementaryView as? DressListHeaderCollectionReusableView {
            header.sortButton?.addTarget(self, action: #selector(openSortSelector), for: .touchUpInside)
 
            if let viewModel = viewModel {
                var buttonTitle = ""
                
                if viewModel.sortBy.count > 0 {
                    buttonTitle.append(viewModel.sortBy.capitalized)
                }
                
                if viewModel.sortOrder.count > 0 {
                    if buttonTitle.count > 0 {
                        buttonTitle.append(" | ")
                    }
                    buttonTitle.append(viewModel.sortOrder)
                }
                
                if buttonTitle.count == 0 {
                    buttonTitle = "Sort options"
                }
                
                header.sortButton?.setTitle(buttonTitle, for: .normal)
            }
        }
        
        return supplementaryView
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let viewModel = viewModel, indexPath.item < viewModel.dresses.count else {
            return
        }
        
        presenter?.presentDressDetail(dress: viewModel.dresses[indexPath.item].id)
    }
}

extension DressListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var columns: Int = 2
        
        if traitCollection.horizontalSizeClass == .regular {
            if UIDevice.current.orientation == .portrait {
                columns = 3
            } else {
                columns = 4
            }
        }
        
        var padding: CGFloat = 0
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            padding = layout.sectionInset.left + layout.sectionInset.right + (layout.minimumInteritemSpacing * CGFloat(columns - 1))
        }
        
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize / CGFloat(columns), height: 310.0)
    }
}

extension DressListViewController: DressListView {
    func updateList(viewModel: DressListViewModel) {
        self.viewModel = viewModel
    }
    
    func displayError(message: String) {
        PKHUD.sharedHUD.contentView = PKHUDErrorView(title: "Error", subtitle: message)
        PKHUD.sharedHUD.show()
        PKHUD.sharedHUD.hide(afterDelay: 2.0)
    }
}
