//
//  DressListViewModel.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct DressListViewModel {
    let dresses: [DressViewModel]
    let canPaginate: Bool
    let sortOrder: String
    let sortBy: String
}
