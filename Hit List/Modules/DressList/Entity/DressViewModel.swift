//
//  DressViewModel.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct DressViewModel {
    let id: String
    let name: String
    let brand: String
    let price: String
    let thumbnailURL: URL?
    let isHitListed: Bool
}
