//
//  DressListDefaultRouter.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class DressListDefaultRouter {
    weak var viewController: UIViewController?
    var dressDetailsBuilder: ((String) -> UIViewController?)?
}

extension DressListDefaultRouter: DressListRouter {
    func navigateToDressDetails(dress id: String) {
        guard let destination = dressDetailsBuilder?(id) else {
            return
        }
        
        viewController?.navigationController?.pushViewController(destination, animated: true)
    }
    
    func openSortPicker(completion: @escaping (DressesRequestSortField?, DressesRequestSortOrder?) -> Void) {
        presentSortBy { [presentSortOrder] selectedField in
            presentSortOrder({ (selectedOrder) in
                completion(selectedField, selectedOrder)
            })
        }
    }
    
    private func presentSortBy(_ completion: @escaping (DressesRequestSortField?) -> Void) {
        let actionSheet = UIAlertController(title: "Sort Field", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Price", style: .default, handler: { _ in
            completion(.price)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Popularity", style: .default, handler: { _ in
            completion(.popularity)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Clear", style: .destructive, handler: { _ in
            completion(nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = actionSheet.popoverPresentationController, let viewController = viewController {
            popoverController.sourceView = viewController.view
            popoverController.sourceRect = CGRect(x: viewController.view.bounds.midX, y: viewController.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        viewController?.present(actionSheet, animated: true, completion: nil)
    }
    
    private func presentSortOrder(_ completion: @escaping (DressesRequestSortOrder?) -> Void) {
        let actionSheet = UIAlertController(title: "Sort Order", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Ascending", style: .default, handler: { _ in
            completion(.ascending)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Descending", style: .default, handler: { _ in
            completion(.descending)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Clear", style: .destructive, handler: { _ in
            completion(nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = actionSheet.popoverPresentationController, let viewController = viewController {
            popoverController.sourceView = viewController.view
            popoverController.sourceRect = CGRect(x: viewController.view.bounds.midX, y: viewController.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        viewController?.present(actionSheet, animated: true, completion: nil)
    }
}
