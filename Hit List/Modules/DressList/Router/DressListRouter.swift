//
//  DressListRouter.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol DressListRouter {
    func navigateToDressDetails(dress id: String)
    func openSortPicker(completion: @escaping (DressesRequestSortField?, DressesRequestSortOrder?) -> Void)
}
