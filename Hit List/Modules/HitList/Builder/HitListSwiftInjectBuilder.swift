//
//  HitListSwiftInjectBuilder.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit
import Swinject

class HitListSwiftInjectBuilder {
    
    private let container: Container
    
    required init(parent: Container) {
        container = Container(parent: parent)
    }
    
    private func registerView() {
        container.register(HitListView.self, factory: { _ in
            HitListViewController(collectionViewLayout: UICollectionViewFlowLayout())
        }).initCompleted({ r, view in
            if let view = view as? HitListViewController {
                view.presenter = r.resolve(HitListPresenter.self)
            }
        }).inObjectScope(.container)
    }
    
    private func registerInteractor() {
        container.register(OperationQueue.self, name: "hitlist_operation_queue", factory: { _ in
            OperationQueue()
        }).initCompleted { (r, queue) in
            queue.qualityOfService = .userInitiated
        }
        
        container.register(HitListInteractor.self, factory: { r in
            HitListDefaultInteractor(hitListService: r.resolve(HitListService.self)!,
                                     dressesService: r.resolve(DressesService.self)!,
                                     queue: r.resolve(OperationQueue.self, name: "hitlist_operation_queue")!)
        }).initCompleted({ r, interactor in
            if let interactor = interactor as? HitListDefaultInteractor {
                interactor.presenter = r.resolve(HitListPresenter.self)
            }
        }).inObjectScope(.container)
    }
    
    private func registerRouter() {
        container.register(HitListRouter.self, factory: { r in
            HitListDefaultRouter()
        }).initCompleted({ r, router in
            if let router = router as? HitListDefaultRouter {
                router.viewController = r.resolve(HitListView.self) as? UIViewController
                router.dressDetailsBuilder = { dressId in
                    let builder = r.resolve(DressDetailsBuilder.self)
                    return builder?.buildDressDetailsModule(dress: dressId)
                }
            }
        })
    }
    
    private func registerPresenter() {
        container.register(HitListPresenter.self, factory: { r in
            HitListDefaultPresenter(interactor: r.resolve(HitListInteractor.self)!,
                                    router: r.resolve(HitListRouter.self)!,
                                    formatter: r.resolve(NumberFormatter.self, name: "currency_formatter")!)
        }).initCompleted({ r, presenter in
            if let presenter = presenter as? HitListDefaultPresenter {
                presenter.view = r.resolve(HitListView.self)
            }
        }).inObjectScope(.container)
    }
}

extension HitListSwiftInjectBuilder: HitListBuilder {
    func buildHitListModule() -> UIViewController? {
        registerView()
        registerInteractor()
        registerRouter()
        registerPresenter()
        
        return container.resolve(HitListView.self) as? UIViewController
    }
}
