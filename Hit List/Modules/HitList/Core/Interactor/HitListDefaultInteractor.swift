//
//  HitListDefaultInteractor.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

class HitListDefaultInteractor {
    weak var presenter: HitListPresenter?
    
    private let hitListService: HitListService
    private let dressesService: DressesService
    
    private var observation: NSKeyValueObservation?
    private var dataSource = [(Line, Dress)]()
    private let operationQueue: OperationQueue
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .didSaveItemToHitList, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didDeleteItemFromHitList, object: nil)
    }
    
    required init(hitListService: HitListService, dressesService: DressesService, queue: OperationQueue) {
        self.hitListService = hitListService
        self.dressesService = dressesService
        self.operationQueue = queue
        
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeHitList), name: .didSaveItemToHitList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeHitList), name: .didDeleteItemFromHitList, object: nil)
    }
    
    @objc func didChangeHitList() {
        loadHitList()
    }
}

extension HitListDefaultInteractor: HitListInteractor {
    func loadHitList() {
        observation = operationQueue.observe(\.operationCount, options: [.new]) { [weak self] (queue, change) in
            guard let `self` = self else { return }
            if change.newValue! == 0 {
                self.presenter?.presentHitList(self.dataSource)
            }
        }
        
        operationQueue.isSuspended = true
        operationQueue.cancelAllOperations()
        
        dataSource.removeAll()
        
        hitListService.getHitlist { [weak self, presenter] (list, error) in
            guard let `self` = self, let list = list else {
                presenter?.presentError(error?.localizedDescription ?? "Unknown error")
                return
            }
            
            guard list.total > 0 else {
                presenter?.presentHitList([])
                return
            }
            
            list.lines.forEach({ (line) in
                let operation = HitListOperation({ operation in
                    self.dressesService.get(dress: line.dressId, { (dress, error) in
                        defer {
                            operation.state = .finished
                        }
                        guard let dress = dress else {
                            return
                        }
                        self.dataSource.append((line, dress))
                    })
                })
                self.operationQueue.addOperation(operation)
            })
            
            self.operationQueue.isSuspended = false
        }
    }
}
