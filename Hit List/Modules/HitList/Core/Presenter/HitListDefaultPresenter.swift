//
//  HitListDefaultPresenter.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation
import PKHUD

class HitListDefaultPresenter {
    weak var view: HitListView?
    
    private let interactor: HitListInteractor
    private let router: HitListRouter
    private let priceFormatter: NumberFormatter
    
    required init(interactor: HitListInteractor, router: HitListRouter, formatter: NumberFormatter) {
        self.interactor = interactor
        self.router = router
        self.priceFormatter = formatter
    }
}

extension HitListDefaultPresenter: HitListPresenter {
    func loadContent() {
        PKHUD.sharedHUD.contentView = PKHUDSystemActivityIndicatorView()
        PKHUD.sharedHUD.show()
        interactor.loadHitList()
    }
    
    func presentHitList(_ hitList: [(Line, Dress)]) {
        PKHUD.sharedHUD.hide()
        let dresses = hitList.map { [priceFormatter] (item) -> DressViewModel in
            let dress = item.1
            let formattedPrice = priceFormatter.string(from: NSNumber(value: dress.price)) ?? "€ -,--"
            var thumbnailURL: URL?
            if let imagePath = dress.images.first {
                thumbnailURL = URL(string: imagePath)
            }
            
            let viewModel = DressViewModel(id: dress.id,
                                           name: dress.name,
                                           brand: dress.brand.name,
                                           price: formattedPrice,
                                           thumbnailURL: thumbnailURL,
                                           isHitListed: true)
            
            return viewModel
        }
        
        let viewModel = DressListViewModel(dresses: dresses, canPaginate: false, sortOrder: "", sortBy: "")
        
        view?.updateList(viewModel: viewModel)
    }
    
    func presentDressDetails(_ id: String) {
        router.navigateToDressDetails(dress: id)
    }
    
    func presentError(_ message: String) {
        PKHUD.sharedHUD.contentView = PKHUDErrorView(title: "Error", subtitle: message)
        if !PKHUD.sharedHUD.isVisible {
            PKHUD.sharedHUD.show()
        }
        PKHUD.sharedHUD.hide(afterDelay: 2.0)
    }
}
