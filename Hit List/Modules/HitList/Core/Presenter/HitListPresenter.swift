//
//  HitListPresenter.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol HitListPresenter: class {
    func loadContent()
    func presentHitList(_ hitList: [(Line, Dress)])
    func presentDressDetails(_ id: String)
    func presentError(_ message: String)
}
