//
//  HitListViewController.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class HitListViewController: UICollectionViewController {
    weak var presenter: HitListPresenter?
    
    private let dressCellId = "DRESS_CELL"
    
    private var viewModel: DressListViewModel? {
        didSet {
            collectionView?.reloadData()
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = .white
        collectionView?.register(UINib(nibName: "DressCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: dressCellId)
        
        title = "Hit List"
        
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 5
            layout.minimumInteritemSpacing = 5
            layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
        
        presenter?.loadContent()
    }
}

extension HitListViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        return viewModel.dresses.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: dressCellId, for: indexPath)
        
        if let dressCell = cell as? DressCollectionViewCell, let dress = viewModel?.dresses[indexPath.item] {
            dressCell.imageView?.image = #imageLiteral(resourceName: "imagePlaceholder")
            dressCell.configure(viewModel: dress)
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let viewModel = viewModel, indexPath.row < viewModel.dresses.count else {
            return
        }
        
        presenter?.presentDressDetails(viewModel.dresses[indexPath.row].id)
    }
}

extension HitListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var columns: Int = 2
        
        if traitCollection.horizontalSizeClass == .regular {
            if UIDevice.current.orientation == .portrait {
                columns = 3
            } else {
                columns = 4
            }
        }
        
        var padding: CGFloat = 0
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            padding = layout.sectionInset.left + layout.sectionInset.right + (layout.minimumInteritemSpacing * CGFloat(columns - 1))
        }
        
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize / CGFloat(columns), height: 310.0)
    }
}

extension HitListViewController: HitListView {
    func updateList(viewModel: DressListViewModel) {
        self.viewModel = viewModel
    }
}
