//
//  HitListDefaultRouter.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class HitListDefaultRouter {
    weak var viewController: UIViewController?
    var dressDetailsBuilder: ((String) -> UIViewController?)?
}

extension HitListDefaultRouter: HitListRouter {
    func navigateToDressDetails(dress id: String) {
        guard let destination = dressDetailsBuilder?(id) else {
            return
        }
        
        viewController?.navigationController?.pushViewController(destination, animated: true)
    }
}
