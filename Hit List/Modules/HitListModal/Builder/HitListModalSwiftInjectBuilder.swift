//
//  HitListModalSwiftInjectBuilder.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit
import Swinject

class HitListModalSwiftInjectBuilder {
    
    private let container: Container
    
    required init(parent: Container) {
        container = Container(parent: parent)
    }
    
    private func registerView() {
        container.register(HitListModalView.self, factory: { _ in
            HitListModalViewController(nibName: "HitListModalViewController", bundle: nil)
        }).initCompleted({ (r, view) in
            if let view = view as? HitListModalViewController {
                view.presenter = r.resolve(HitListModalPresenter.self)
            }
        }).inObjectScope(.container)
    }
    
    private func registerInteractor(dress: Dress, line: Line?) {
        container.register(HitListModalInteractor.self, factory: { r in
            HitListModalDefaultInteractor(hitListService: r.resolve(HitListService.self)!,
                                          dress: dress,
                                          line: line)
        }).initCompleted({ r, interactor in
            if let interactor = interactor as? HitListModalDefaultInteractor {
                interactor.presenter = r.resolve(HitListModalPresenter.self)
            }
        }).inObjectScope(.container)
    }
    
    private func registerPresenter() {
        container.register(HitListModalPresenter.self, factory: { r in
            HitListModalDefaultPresenter(interactor: r.resolve(HitListModalInteractor.self)!)
        }).initCompleted({ r, presenter in
            if let presenter = presenter as? HitListModalDefaultPresenter {
                presenter.view = r.resolve(HitListModalView.self)
            }
        })
    }
}

extension HitListModalSwiftInjectBuilder: HitListModalBuilder {
    func buildModule(dress: Dress, line: Line?) -> UIViewController? {
        registerView()
        registerInteractor(dress: dress, line: line)
        registerPresenter()
        
        return container.resolve(HitListModalView.self) as? UIViewController
    }
}
