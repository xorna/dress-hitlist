//
//  HitListModalDefaultInteractor.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let didSaveItemToHitList = Notification.Name(rawValue: "did_save_item_to_hit_list")
    static let didDeleteItemFromHitList = Notification.Name(rawValue: "did_delete_item_from_hit_list")
}

class HitListModalDefaultInteractor {
    
    private let hitListService: HitListService
    private let dress: Dress
    private var line: Line?
    
    weak var presenter: HitListModalPresenter?
    
    required init(hitListService: HitListService, dress: Dress, line: Line?) {
        self.hitListService = hitListService
        self.dress = dress
        self.line = line
    }
    
    private func saveNewLine(rating: Int) {
        let newLine = Line(id: nil, dressId: dress.id, rating: rating)
        hitListService.save(line: newLine) { (updatedLine, error) in
            guard let updateLine = updatedLine else {
                // error handling
                return
            }
            NotificationCenter.default.post(name: .didSaveItemToHitList, object: updateLine)
        }
    }
    
    private func updateLine(line: Line) {
        hitListService.update(line: line) { (line, error) in
            guard let line = line else {
                // error handling
                return
            }
            NotificationCenter.default.post(name: .didSaveItemToHitList, object: line)
        }
    }
}

extension HitListModalDefaultInteractor: HitListModalInteractor {
    func loadDressLine() {
        presenter?.present(dress: dress, line: line)
    }
    
    func saveLine(rating: Int) {
        guard let line = line, line.id != nil else {
            return saveNewLine(rating: rating)
        }
        let updatedLine = Line(id: line.id, dressId: line.dressId, rating: rating)
        updateLine(line: updatedLine)
    }
    
    func deleteLine() {
        guard let lineId = line?.id else {
            return
        }
        
        hitListService.delete(line: lineId) { (error) in
            guard error == nil else {
                return
            }
            NotificationCenter.default.post(name: .didDeleteItemFromHitList, object: lineId)
        }
    }
}
