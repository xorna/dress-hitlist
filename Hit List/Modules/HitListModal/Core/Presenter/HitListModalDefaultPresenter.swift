//
//  HitListModalDefaultPresenter.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

class HitListModalDefaultPresenter {
    
    private let interactor: HitListModalInteractor
    
    weak var view: HitListModalView?
    
    required init(interactor: HitListModalInteractor) {
        self.interactor = interactor
    }
}

extension HitListModalDefaultPresenter: HitListModalPresenter {
    func loadContent() {
        interactor.loadDressLine()
    }
    
    func saveToHitList(rating: Int) {
        interactor.saveLine(rating: rating)
    }
    
    func deleteFromHitList() {
        interactor.deleteLine()
    }
    
    func present(dress: Dress, line: Line?) {
        let viewModel = HitListModalViewModel(title: dress.name, rating: line?.rating ?? 0)
        DispatchQueue.main.async { [view] in
            view?.update(viewModel: viewModel)
        }
    }
}
