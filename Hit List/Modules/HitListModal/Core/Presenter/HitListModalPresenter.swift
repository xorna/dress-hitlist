//
//  HitListModalPresenter.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol HitListModalPresenter: class {
    func loadContent()
    func saveToHitList(rating: Int)
    func deleteFromHitList()
    func present(dress: Dress, line: Line?)
}
