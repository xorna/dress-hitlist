//
//  HitListModalView.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol HitListModalView: class {
    func update(viewModel: HitListModalViewModel)
}
