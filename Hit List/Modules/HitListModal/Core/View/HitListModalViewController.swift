//
//  HitListModalViewController.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class HitListModalViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel?
    @IBOutlet weak var slider: TickSlider?
    
    var presenter: HitListModalPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.loadContent()
    }
    
    func deleteAction() {
        presenter?.deleteFromHitList()
    }
    
    func saveAction() {
        presenter?.saveToHitList(rating: Int(slider?.value ?? 0))
    }
}

extension HitListModalViewController: HitListModalView {
    func update(viewModel: HitListModalViewModel) {
        label?.text = viewModel.title
        slider?.value = Float(viewModel.rating)
    }
}
