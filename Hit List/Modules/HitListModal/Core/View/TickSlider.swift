//
//  TickSlider.swift
//  Hit List
//
//  Created by Andre Gustavo on 29/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import UIKit

class TickSlider: UISlider {
    override var value: Float {
        get { return round(super.value) }
        set { super.value = round(newValue) }
    }
}
