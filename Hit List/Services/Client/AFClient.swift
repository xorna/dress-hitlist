//
//  AFClient.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation
import Alamofire

class AFClient {
    
    private let baseURL: URL
    private let decoder: JSONDecoder
    private let encoder: JSONEncoder
    private let defaultHeaders = ["Accept":"application/json", "Content-Type":"application/json"]
    
    required init(baseURL: URL, decoder: JSONDecoder, encoder: JSONEncoder) {
        self.baseURL = baseURL
        self.decoder = decoder
        self.encoder = encoder
    }
    
}

extension AFClient: Client {
    func get<T: Codable>(endpoint: String, params: [String:Any]?, completion: @escaping ClientCompletion<T>) {
        let url = baseURL.appendingPathComponent(endpoint)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: defaultHeaders).responseData { [decoder] (response) in
            guard let data = response.data else {
                completion(nil, FTError(localizedDescription: response.error?.localizedDescription ?? "Unknown network error"))
                return
            }
            
            do {
                let result = try decoder.decode(T.self, from: data)
                completion(result, nil)
            } catch {
                do {
                    let error: FTError = try decoder.decode(FTError.self, from: data)
                    completion(nil, error)
                } catch {
                    completion(nil, FTError(localizedDescription: error.localizedDescription))
                }
            }
        }
    }
    
    func post<T: Codable, B: Codable>(endpoint: String, body: B?, completion: @escaping (T?, FTError?) -> Void) {
        let url = baseURL.appendingPathComponent(endpoint)
        do {
            var httpRequest = try URLRequest(url: url, method: .post, headers: defaultHeaders)
            let encoder = JSONEncoder()
            if let body = body {
                let bodyData: Data = try encoder.encode(body)
                httpRequest.httpBody = bodyData
            }
            Alamofire.request(httpRequest).responseData { [decoder] (response) in
                guard let data = response.data else {
                    completion(nil, FTError(localizedDescription: response.error?.localizedDescription ?? "Unknown network error"))
                    return
                }
                
                do {
                    let result = try decoder.decode(T.self, from: data)
                    completion(result, nil)
                } catch {
                    do {
                        let error: FTError = try decoder.decode(FTError.self, from: data)
                        completion(nil, error)
                    } catch {
                        completion(nil, FTError(localizedDescription: error.localizedDescription))
                    }
                }
            }
        } catch {
            completion(nil, FTError(localizedDescription: error.localizedDescription))
            return
        }
    }
    
    func put<T: Codable, B: Codable>(endpoint: String, body: B?, completion: @escaping ClientCompletion<T>) {
        let url = baseURL.appendingPathComponent(endpoint)
        do {
            var httpRequest = try URLRequest(url: url, method: .put, headers: defaultHeaders)
            let encoder = JSONEncoder()
            if let body = body {
                let bodyData: Data = try encoder.encode(body)
                httpRequest.httpBody = bodyData
            }
            Alamofire.request(httpRequest).responseData { [decoder] (response) in
                guard let data = response.data else {
                    completion(nil, FTError(localizedDescription: response.error?.localizedDescription ?? "Unknown network error"))
                    return
                }
                
                do {
                    let result = try decoder.decode(T.self, from: data)
                    completion(result, nil)
                } catch {
                    do {
                        let error: FTError = try decoder.decode(FTError.self, from: data)
                        completion(nil, error)
                    } catch {
                        completion(nil, FTError(localizedDescription: error.localizedDescription))
                    }
                }
            }
        } catch {
            completion(nil, FTError(localizedDescription: error.localizedDescription))
            return
        }
    }
    
    func delete(endpoint: String, params: [String : Any]?, completion: @escaping (FTError?) -> Void) {
        let url = baseURL.appendingPathComponent(endpoint)
        Alamofire.request(url, method: .delete, parameters: nil, encoding: URLEncoding(), headers: defaultHeaders).responseData { (response) in
            if let error = response.error {
                return completion(FTError(localizedDescription: error.localizedDescription))
            }
            completion(nil)
        }
    }
}
