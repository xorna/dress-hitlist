//
//  Client.swift
//  Hit List
//
//  Created by Andre Gustavo on 25/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

typealias ClientCompletion<T> = (T?, FTError?) -> Void

protocol Client {
    func get<T: Codable>(endpoint: String, params: [String:Any]?, completion: @escaping ClientCompletion<T>)
    func post<T: Codable, B: Codable>(endpoint: String, body: B?, completion: @escaping (T?, FTError?) -> Void)
    func put<T: Codable, B: Codable>(endpoint: String, body: B?, completion: @escaping ClientCompletion<T>)
    func delete(endpoint: String, params: [String:Any]?, completion: @escaping (FTError?) -> Void)
}
