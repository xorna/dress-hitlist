//
//  DefaultDressesService.swift
//  Hit List
//
//  Created by Andre Gustavo on 25/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

class DefaultDressesService {
    private let client: Client
    
    required init(client: Client) {
        self.client = client
    }
}

extension DefaultDressesService: DressesService {
    func getAll(request: DressesRequest, _ completion: @escaping (DressList?, FTError?) -> Void) {
        client.get(endpoint: "dresses", params: request.dictionary, completion: completion)
    }
    
    func get(dress id: String, _ completion: @escaping (Dress?, FTError?) -> Void) {
        let endpoint = "dresses/\(id)"
        client.get(endpoint: endpoint, params: nil, completion: completion)
    }
}
