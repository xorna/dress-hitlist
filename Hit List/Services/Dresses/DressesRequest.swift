//
//  DressesRequest.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

struct DressesRequest {
    let pageSize: Int
    let pageNum: Int
    var sortOn: DressesRequestSortField?
    var sortOrder: DressesRequestSortOrder?
    
    var dictionary: [String:Any] {
        var dictionary: [String:Any] = [
            "pageSize" : pageSize,
            "pageNum" : pageNum
        ]
        
        if let sortOn = sortOn {
            dictionary["sortOn"] = sortOn.rawValue
        }
        
        if let sortOrder = sortOrder {
            dictionary["sortOrder"] = sortOrder.rawValue
        }
        
        return dictionary
    }
}
