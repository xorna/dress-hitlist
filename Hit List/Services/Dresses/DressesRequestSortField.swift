//
//  DressesRequestSortField.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

enum DressesRequestSortField: String {
    case price
    case popularity
}
