//
//  DressesService.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol DressesService {
    func getAll(request: DressesRequest, _ completion: @escaping (DressList?, FTError?) -> Void)
    func get(dress id: String, _ completion: @escaping (Dress?, FTError?) -> Void)
}
