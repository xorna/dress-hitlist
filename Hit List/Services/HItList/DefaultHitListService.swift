//
//  DefaultHitListService.swift
//  Hit List
//
//  Created by Andre Gustavo on 26/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

class DefaultHitListService {
    private let client: Client
    
    required init(client: Client) {
        self.client = client
    }
}

extension DefaultHitListService: HitListService {
    func getHitlist(_ completion: @escaping (HitList?, FTError?) -> Void) {
        client.get(endpoint: "hitlist", params: nil, completion: completion)
    }
    
    func get(line id: String, _ completion: @escaping (Line?, FTError?) -> Void) {
        let endpoint = "hitlist/lines/\(id)"
        client.get(endpoint: endpoint, params: nil, completion: completion)
    }
    
    func save(line: Line, _ completion: @escaping (Line?, FTError?) -> Void) {
       client.post(endpoint: "hitlist/lines", body: line, completion: completion)
    }
    
    func delete(line id: String, _ completion: @escaping (FTError?) -> Void) {
        let endpoint = "hitlist/lines/\(id)"
        client.delete(endpoint: endpoint, params: nil, completion: completion)
    }
    
    func update(line: Line, _ completion: @escaping (Line?, FTError?) -> Void) {
        guard let id = line.id else {
            completion(nil, FTError(localizedDescription: "Id not found"))
            return
        }
        let endpoint = "hitlist/lines/\(id)"
        client.put(endpoint: endpoint, body: line, completion: completion)
    }
}
