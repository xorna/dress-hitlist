//
//  HitListService.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol HitListService {
    func getHitlist(_ completion: @escaping (HitList?, FTError?) -> Void)
    func get(line id: String, _ completion: @escaping (Line?, FTError?) -> Void)
    func save(line: Line, _ completion: @escaping (Line?, FTError?) -> Void)
    func delete(line id: String, _ completion: @escaping (FTError?) -> Void)
    func update(line: Line, _ completion: @escaping (Line?, FTError?) -> Void)
}
