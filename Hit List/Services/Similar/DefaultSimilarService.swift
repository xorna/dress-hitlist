//
//  DefaultSimilarService.swift
//  Hit List
//
//  Created by Andre Gustavo on 26/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

class DefaultSimilarService {
    private let client: Client
    
    required init(client: Client) {
        self.client = client
    }
}

extension DefaultSimilarService: SimilarService {
    func find(id: String, by: Similarity, _ completion: @escaping (DressList?, FTError?) -> Void) {
        let endpoint = "similar/\(by.rawValue)/\(id)"
        client.get(endpoint: endpoint, params: nil, completion: completion)
    }
}
