//
//  SimilarService.swift
//  Hit List
//
//  Created by Andre Gustavo on 24/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation

protocol SimilarService {
    func find(id: String, by: Similarity, _ completion: @escaping (DressList?, FTError?) -> Void)
}
