//
//  SharedContainer.swift
//  Hit List
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation
import Swinject

extension Container {
    static let shared: Container = {
        let container = Container()
        
        registerVariables(container: container)
        registerUtilities(container: container)
        registerClient(container: container)
        registerServices(container: container)
        registerBuilders(container: container)
        registerMainModule(container: container)
        
        return container
    }()
    
    private static func registerVariables(container: Container) {
        guard let baseURLString = Bundle.main.object(forInfoDictionaryKey: "BASE_URL") as? String else {
            return
        }
        container.register(URL.self, name: "api_url", factory: { _ in
            return URL(string: baseURLString)!
        }).inObjectScope(.container)
    }
    
    private static func registerUtilities(container: Container) {
        container.register(NumberFormatter.self, name: "currency_formatter", factory: { _ in
            NumberFormatter()
        }).initCompleted({ (r, formatter) in
            formatter.numberStyle = .currency
            formatter.locale = NSLocale.current
        }).inObjectScope(.container)
        
        container.register(JSONDecoder.self, factory: { _ in
            JSONDecoder()
        }).inObjectScope(.container)
        
        container.register(JSONEncoder.self, factory: { _ in
            JSONEncoder()
        }).inObjectScope(.container)
    }
    
    private static func registerClient(container: Container) {
        container.register(Client.self, factory: { r in
            AFClient(baseURL: r.resolve(URL.self, name: "api_url")!,
                     decoder: r.resolve(JSONDecoder.self)!,
                     encoder: r.resolve(JSONEncoder.self)!)
        }).inObjectScope(.container)
    }
    
    private static func registerServices(container: Container) {
        container.register(SimilarService.self, factory: { r in
            DefaultSimilarService(client: r.resolve(Client.self)!)
        }).inObjectScope(.container)
        
        container.register(HitListService.self, factory: { r in
            DefaultHitListService(client: r.resolve(Client.self)!)
        }).inObjectScope(.container)
        
        container.register(DressesService.self, factory: { r in
            DefaultDressesService(client: r.resolve(Client.self)!)
        }).inObjectScope(.container)
    }
    
    private static func registerBuilders(container: Container) {
        container.register(DressListBuilder.self, factory: { _ in
            DressListSwiftInjectBuilder(parent: container)
        }).inObjectScope(.container)
        
        container.register(DressDetailsBuilder.self, factory: { _ in
            DressDetailsSwiftInjectBuilder(parent: container)
        }).inObjectScope(.container)
        
        container.register(HitListModalBuilder.self, factory: { _ in
            HitListModalSwiftInjectBuilder(parent: container)
        }).inObjectScope(.container)
        
        container.register(HitListBuilder.self, factory: { _ in
            HitListSwiftInjectBuilder(parent: container)
        }).inObjectScope(.container)
    }
    
    private static func registerMainModule(container: Container) {
        container.register(UITabBarController.self, factory: { r in
            UITabBarController()
        }).initCompleted({ r, controller in
            controller.viewControllers = [
                {
                    let navigation = UINavigationController(rootViewController: r.resolve(DressListBuilder.self)!.buildDressListModule()!)
                    navigation.tabBarItem = UITabBarItem(title: "Dresses", image: #imageLiteral(resourceName: "DressIcon"), tag: 0)
                    return navigation
                }(),
                {
                    let navigation = UINavigationController(rootViewController: r.resolve(HitListBuilder.self)!.buildHitListModule()!)
                    navigation.tabBarItem = UITabBarItem(title: "Hit List", image: #imageLiteral(resourceName: "HitListIcon"), tag: 1)
                    return navigation
                }()
            ]
        })
    }
}
