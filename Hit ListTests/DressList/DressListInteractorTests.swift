//
//  DressListInteractorTests.swift
//  Hit ListTests
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import XCTest
@testable import Hit_List

class DressListInteractorTests: XCTestCase {
    
    private var interactor: DressListDefaultInteractor!
    private var dressesService: DressListServiceDouble!
    private var hitListService: HitListServiceDouble!
    private var presenter: DressListPresenterDouble!
    
    override func setUp() {
        super.setUp()
        
        dressesService = DressListServiceDouble()
        hitListService = HitListServiceDouble()
        presenter = DressListPresenterDouble()
        interactor = DressListDefaultInteractor(dressesService: dressesService, hitListService: hitListService)
        interactor.presenter = presenter
    }
    
    override func tearDown() {
        interactor = nil
        presenter = nil
        dressesService = nil
        hitListService = nil
        super.tearDown()
    }
    
    func testSuccessfulLoadAndSendDressesToPresenter() {
        let dress = DressListItem(id: "ZA821C04I-B11",
                                  brandName: "Zalando Essentials",
                                  name: "Jersey dress - beige",
                                  price: 9.59,
                                  thumbnails: ["http://i4.ztat.net/catalog_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@13.jpg",
                                               "http://i5.ztat.net/catalog_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@12.jpg"])
        let dressList = DressList(items: [dress], pageCount: 10)
        
        dressesService.dataSource = (dressList, nil)
        
        let line = Line(id: "146506f6-d97f-45c4-9e0e-19461d2aa787", dressId: "ZA821C04I-B11", rating: 4)
        let list = HitList(rating: 4, lines: [line], total: 1)
        hitListService.dataSource = (list, nil)
        
        let loadExpectation = expectation(description: "load_dresses")
        
        interactor.loadDresses(page: 1, sortOn: .popularity, sortOrder: .ascending)
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) { [dressesService, hitListService, presenter] in
            
            XCTAssertEqual(dressesService?.receivedPage, 1)
            XCTAssertEqual(dressesService?.receivedPageSize, 20)
            XCTAssertEqual(dressesService?.receivedSortOrder, DressesRequestSortOrder.ascending)
            XCTAssertEqual(dressesService?.receivedSortOn, DressesRequestSortField.popularity)
            
            XCTAssertEqual(hitListService?.receivedRequest, true)
            
            XCTAssertNotNil(presenter?.appendedDresses)
            XCTAssertEqual(presenter?.appendedDresses, dressList)
            XCTAssertNotNil(presenter?.hitList)
            XCTAssertEqual(presenter?.hitList, list)
            XCTAssertNotNil(presenter?.page)
            XCTAssertEqual(presenter?.page, 1)
            XCTAssertNil(presenter?.error)
            
            loadExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedLoadDressesAndSendErrorToPresenter() {
        dressesService.dataSource = (nil, .notFound)
        
        let failedToloadExpectation = expectation(description: "failed_load_dresses")
        
        interactor.loadDresses(page: 1, sortOn: .popularity, sortOrder: .ascending)
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) { [dressesService, hitListService, presenter] in
            
            XCTAssertEqual(dressesService?.receivedPage, 1)
            XCTAssertEqual(dressesService?.receivedPageSize, 20)
            XCTAssertEqual(dressesService?.receivedSortOrder, DressesRequestSortOrder.ascending)
            XCTAssertEqual(dressesService?.receivedSortOn, DressesRequestSortField.popularity)
            
            XCTAssertNil(hitListService?.receivedRequest)
            
            XCTAssertNil(presenter?.appendedDresses)
            XCTAssertNil(presenter?.hitList)
            XCTAssertNil(presenter?.page)
            XCTAssertNotNil(presenter?.error)
            XCTAssertEqual(presenter?.error, FTError.notFound)
            
            failedToloadExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedLoadHitListAndSendErrorToPresenter() {
        let dress = DressListItem(id: "ZA821C04I-B11",
                                  brandName: "Zalando Essentials",
                                  name: "Jersey dress - beige",
                                  price: 9.59,
                                  thumbnails: ["http://i4.ztat.net/catalog_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@13.jpg",
                                               "http://i5.ztat.net/catalog_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@12.jpg"])
        let dressList = DressList(items: [dress], pageCount: 10)
        
        dressesService.dataSource = (dressList, nil)
        
        hitListService.dataSource = (nil, .notFound)
        
        let failedToLoadExpectation = expectation(description: "failed_load_hitlist")
        
        interactor.loadDresses(page: 1, sortOn: .popularity, sortOrder: .ascending)
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) { [dressesService, hitListService, presenter] in
            
            XCTAssertEqual(dressesService?.receivedPage, 1)
            XCTAssertEqual(dressesService?.receivedPageSize, 20)
            XCTAssertEqual(dressesService?.receivedSortOrder, DressesRequestSortOrder.ascending)
            XCTAssertEqual(dressesService?.receivedSortOn, DressesRequestSortField.popularity)
            
            XCTAssertEqual(hitListService?.receivedRequest, true)
            
            XCTAssertNil(presenter?.appendedDresses)
            XCTAssertNil(presenter?.hitList)
            XCTAssertNil(presenter?.page)
            XCTAssertNotNil(presenter?.error)
            XCTAssertEqual(presenter?.error, FTError.notFound)
            
            failedToLoadExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
}

private class DressListServiceDouble: DressesService {
    var dataSource: (DressList?, FTError?)!
    
    var receivedPage: Int?
    var receivedPageSize: Int?
    var receivedSortOn: DressesRequestSortField?
    var receivedSortOrder: DressesRequestSortOrder?
    
    func getAll(request: DressesRequest, _ completion: @escaping (DressList?, FTError?) -> Void) {
        receivedPage = request.pageNum
        receivedPageSize = request.pageSize
        receivedSortOn = request.sortOn
        receivedSortOrder = request.sortOrder
        completion(dataSource.0, dataSource.1)
    }
    
    func get(dress id: String, _ completion: @escaping (Dress?, FTError?) -> Void) { }
}

private class HitListServiceDouble: HitListService {
    var dataSource: (HitList?, FTError?)!
    
    var receivedRequest: Bool?
    
    func getHitlist(_ completion: @escaping (HitList?, FTError?) -> Void) {
        receivedRequest = true
        completion(dataSource.0, dataSource.1)
    }
    
    func get(line id: String, _ completion: @escaping (Line?, FTError?) -> Void) { }
    func save(line: Line, _ completion: @escaping (Line?, FTError?) -> Void) { }
    func delete(line id: String, _ completion: @escaping (FTError?) -> Void) { }
    func update(line: Line, _ completion: @escaping (Line?, FTError?) -> Void) { }
}

private class DressListPresenterDouble: DressListPresenter {
    
    var appendedDresses: DressList?
    var hitList: HitList?
    var page: Int?
    var error: FTError?
    
    func append(dresses: DressList, hitList: HitList, page: Int) {
        self.appendedDresses = dresses
        self.hitList = hitList
        self.page = page
    }
    
    func presentError(_ error: FTError) {
        self.error = error
    }
    
    func loadContent() { }
    func loadNexPage() { }
    func presentDressDetail(dress id: String) { }
    func presentSortPicker() { }
    func changeSort(by field: DressesRequestSortField?, order: DressesRequestSortOrder?) { }
}
