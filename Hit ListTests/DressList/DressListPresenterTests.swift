//
//  DressListPresenterTests.swift
//  Hit ListTests
//
//  Created by Andre Gustavo on 27/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import XCTest
@testable import Hit_List

class DressListPresenterTests: XCTestCase {
    
    private var formatter: NumberFormatter!
    private var interactor: DressListInteractorDouble!
    private var router: DressListRouterDouble!
    private var view: DressListViewDouble!
    private var presenter: DressListDefaultPresenter!
    
    override func setUp() {
        super.setUp()
        
        formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "NL")
        formatter.currencySymbol = "€"
        
        interactor = DressListInteractorDouble()
        router = DressListRouterDouble()
        view = DressListViewDouble()
        
        presenter = DressListDefaultPresenter(interactor: interactor, router: router, currencyFormatter: formatter)
        presenter.view = view
    }
    
    override func tearDown() {
        presenter = nil
        router = nil
        interactor = nil
        formatter = nil
        super.tearDown()
    }
    
    func testLoadContent() {
        presenter.loadContent()
        XCTAssertNotNil(interactor.receivedPage)
        XCTAssertEqual(interactor.receivedPage, 1)
        XCTAssertNil(interactor.receivedSortOn)
        XCTAssertNil(interactor.receivedSortOrder)
        
    }
    
    func testLoadNextPage() {
        presenter.loadNexPage()
        XCTAssertNotNil(interactor.receivedPage)
        XCTAssertEqual(interactor.receivedPage, 2)
        XCTAssertNil(interactor.receivedSortOn)
        XCTAssertNil(interactor.receivedSortOrder)
        
        presenter.loadNexPage()
        XCTAssertNotNil(interactor.receivedPage)
        XCTAssertEqual(interactor.receivedPage, 3)
        XCTAssertNil(interactor.receivedSortOn)
        XCTAssertNil(interactor.receivedSortOrder)
    }
    
    func testAppedingNewContent() {
        let dressA = DressListItem(id: "ZA821C04I-B11",
                                   brandName: "Zalando Essentials",
                                   name: "Jersey dress - beige",
                                   price: 9.59,
                                   thumbnails: ["http://i4.ztat.net/catalog_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@13.jpg",
                                                "http://i5.ztat.net/catalog_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@12.jpg"])
        let dressB = DressListItem(id: "ZA821C04I-K11",
                                   brandName: "Zalando Essentials",
                                   name: "Jersey dress - dark blue",
                                   price: 9.59,
                                   thumbnails: ["http://i2.ztat.net/catalog_hd/ZA/82/1C/04/IK/11/ZA821C04I-K11@12.jpg",
                                                "http://i4.ztat.net/catalog_hd/ZA/82/1C/04/IK/11/ZA821C04I-K11@3.jpg"])
        
        
        let dressList = DressList(items: [dressA, dressB], pageCount: 10)
        
        let line = Line(id: "146506f6-d97f-45c4-9e0e-19461d2aa787", dressId: "ZA821C04I-B11", rating: 4)
        let list = HitList(rating: 4, lines: [line], total: 1)
        
        let contentLoadExpectation = expectation(description: "load_content")
        
        presenter.append(dresses: dressList, hitList: list, page: 1)
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) { [view] in
            XCTAssertNil(view!.receivedErrorMessage)
            XCTAssertNotNil(view!.receivedViewModel)
            XCTAssertEqual(view!.receivedViewModel?.canPaginate, true)
            XCTAssertEqual(view!.receivedViewModel?.dresses.count, 2)
            XCTAssertEqual(view!.receivedViewModel?.sortBy, "")
            XCTAssertEqual(view!.receivedViewModel?.sortOrder, "")

            let firstDress = view?.receivedViewModel?.dresses.first
            XCTAssertNotNil(firstDress)
            XCTAssertEqual(firstDress?.id, "ZA821C04I-B11")
            XCTAssertEqual(firstDress?.brand, "Zalando Essentials")
            XCTAssertEqual(firstDress?.name, "Jersey dress - beige")
            XCTAssertEqual(firstDress?.price, "€\u{00a0}9,59")
            XCTAssertEqual(firstDress?.thumbnailURL, URL(string: "http://i4.ztat.net/catalog_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@13.jpg"))
            XCTAssertEqual(firstDress?.isHitListed, true)
            
            let secondDress = view?.receivedViewModel?.dresses.last
            XCTAssertNotNil(secondDress)
            XCTAssertEqual(secondDress?.id, "ZA821C04I-K11")
            XCTAssertEqual(secondDress?.brand, "Zalando Essentials")
            XCTAssertEqual(secondDress?.name, "Jersey dress - dark blue")
            XCTAssertEqual(secondDress?.price, "€\u{00a0}9,59")
            XCTAssertEqual(secondDress?.thumbnailURL, URL(string: "http://i2.ztat.net/catalog_hd/ZA/82/1C/04/IK/11/ZA821C04I-K11@12.jpg"))
            XCTAssertEqual(secondDress?.isHitListed, false)
            
            contentLoadExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testPresentError() {
        
        let errorExpectation = expectation(description: "error_message")
        
        presenter.presentError(.notFound)
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) { [view] in
            XCTAssertNotNil(view?.receivedErrorMessage)
            XCTAssertEqual(view?.receivedErrorMessage, "File not found")
            
            errorExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testChangedSort() {
        presenter.changeSort(by: .popularity, order: .ascending)
        XCTAssertNotNil(interactor.receivedSortOrder)
        XCTAssertNotNil(interactor.receivedSortOn)
        XCTAssertEqual(interactor.receivedSortOrder, DressesRequestSortOrder.ascending)
        XCTAssertEqual(interactor.receivedSortOn, DressesRequestSortField.popularity)
        XCTAssertEqual(interactor.receivedPage, 1)
        
        presenter.changeSort(by: nil, order: nil)
        XCTAssertNil(interactor.receivedSortOn)
        XCTAssertNil(interactor.receivedSortOrder)
        XCTAssertEqual(interactor.receivedPage, 1)
        
        presenter.changeSort(by: .price, order: .descending)
        XCTAssertNotNil(interactor.receivedSortOrder)
        XCTAssertNotNil(interactor.receivedSortOn)
        XCTAssertEqual(interactor.receivedSortOrder, DressesRequestSortOrder.descending)
        XCTAssertEqual(interactor.receivedSortOn, DressesRequestSortField.price)
        XCTAssertEqual(interactor.receivedPage, 1)
    }
    
    func testPresentDressDetails() {
        presenter.presentDressDetail(dress: "ZA821C04I-B11")
        XCTAssertNotNil(router.receivedDressId)
        XCTAssertEqual(router.receivedDressId, "ZA821C04I-B11")
    }
    
    func testPresentSortPicker() {
        presenter.presentSortPicker()
        XCTAssertNotNil(router.receivedPickerRequest)
        XCTAssertEqual(router.receivedPickerRequest, true)
    }
}

private class DressListInteractorDouble: DressListInteractor {
    
    var receivedPage: Int?
    var receivedSortOn: DressesRequestSortField?
    var receivedSortOrder: DressesRequestSortOrder?
    
    func loadDresses(page: Int, sortOn: DressesRequestSortField?, sortOrder: DressesRequestSortOrder?) {
        receivedPage = page
        receivedSortOn = sortOn
        receivedSortOrder = sortOrder
    }
}

private class DressListRouterDouble: DressListRouter {
    var receivedDressId: String?
    var receivedPickerRequest: Bool?
    
    func navigateToDressDetails(dress id: String) {
        receivedDressId = id
    }
    
    func openSortPicker(completion: @escaping (DressesRequestSortField?, DressesRequestSortOrder?) -> Void) {
        receivedPickerRequest = true
    }
}

private class DressListViewDouble: DressListView {
    var receivedViewModel: DressListViewModel?
    var receivedErrorMessage: String?
    
    func updateList(viewModel: DressListViewModel) {
        receivedViewModel = viewModel
    }
    
    func displayError(message: String) {
        receivedErrorMessage = message
    }
}
