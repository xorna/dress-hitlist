//
//  DefaultDressListServiceTests.swift
//  Hit ListTests
//
//  Created by Andre Gustavo on 26/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import XCTest
@testable import Hit_List

class DefaultDressListServiceTests: XCTestCase {
    
    var client: ClientDouble!
    var service: DefaultDressesService!
    
    var failingClient: ClientDouble!
    var failingService: DefaultDressesService!
    
    override func setUp() {
        super.setUp()
        let fileMap = [
            "get_dresses" : "dresses",
            "get_dresses/ZA821C04I-B11" : "dressDetails"
        ]
        client = ClientDouble(fileMap: fileMap)
        service = DefaultDressesService(client: client)
        
        let fakeFileMap = [
            "get_dresses" : "notFound",
            "get_dresses/ZA821C04I-B11" : "notFound"
        ]
        
        failingClient = ClientDouble(fileMap: fakeFileMap)
        failingService = DefaultDressesService(client: failingClient)
    }
    
    override func tearDown() {
        service = nil
        failingService = nil
        client = nil
        failingClient = nil
        super.tearDown()
    }
    
    func testDressListSuccessfulLoad() {
        
        let request = DressesRequest(pageSize: 20, pageNum: 1, sortOn: .price, sortOrder: nil)
        
        let listExpectation = expectation(description: "dress_list")
        
        service.getAll(request: request) { (list, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(list)
            XCTAssertEqual(list?.items.count, 20)
            XCTAssertEqual(list?.pageCount, 437)
            XCTAssertEqual(list?.items.first?.id, "ZA821C04I-B11")
            
            listExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedEndpoint[.get], "dresses")
        
        if let params = client.receivedParams[.get] as? [String:Any] {
            let expectedParameters = request.dictionary
            XCTAssertEqual(params["pageSize"] as? Int, expectedParameters["pageSize"] as? Int)
            XCTAssertEqual(params["pageNum"] as? Int, expectedParameters["pageNum"] as? Int)
            XCTAssertEqual(params["sortOn"] as? String, expectedParameters["sortOn"] as? String)
            XCTAssertEqual(params["sortOrder"] as? String, expectedParameters["sortOrder"] as? String)
        } else {
            XCTAssertNotNil(nil)
        }
        
        waitForExpectations(timeout: 1) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testDressListFailedLoad() {
        let failedExpectation = expectation(description: "failed_dress_list")
        
        let request = DressesRequest(pageSize: 20, pageNum: 1, sortOn: .price, sortOrder: nil)
        failingService.getAll(request: request) { (list, error) in
            XCTAssertNil(list)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            
            failedExpectation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedEndpoint[.get], "dresses")
        
        if let params = failingClient.receivedParams[.get] as? [String:Any] {
            let expectedParameters = request.dictionary
            XCTAssertEqual(params["pageSize"] as? Int, expectedParameters["pageSize"] as? Int)
            XCTAssertEqual(params["pageNum"] as? Int, expectedParameters["pageNum"] as? Int)
            XCTAssertEqual(params["sortOn"] as? String, expectedParameters["sortOn"] as? String)
            XCTAssertEqual(params["sortOrder"] as? String, expectedParameters["sortOrder"] as? String)
        } else {
            XCTAssertNotNil(nil)
        }
        
        waitForExpectations(timeout: 1) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testDressDetailsSuccessfulLoad() {
        
        let dressId = "ZA821C04I-B11"
        
        let dressExpectation = expectation(description: "dress_details")
        
        service.get(dress: dressId) { (dress, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(dress)
            
            XCTAssertEqual(dress?.brand.logo, "https://i3.ztat.net/brand/zalando-essentials.jpg")
            XCTAssertEqual(dress?.brand.name, "Zalando Essentials")
            XCTAssertEqual(dress?.color, "Beige")
            XCTAssertEqual(dress?.id, "ZA821C04I-B11")
            XCTAssertEqual(dress?.name, "Jersey dress - beige")
            XCTAssertEqual(dress?.price, 9.59)
            XCTAssertEqual(dress?.season, "WINTER")
            XCTAssertEqual(dress?.images.count, 7)
            XCTAssert(dress?.images.contains("http://i4.ztat.net/large_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@13.jpg") ?? false)
            XCTAssert(dress?.images.contains("http://i5.ztat.net/large_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@12.jpg") ?? false)
            XCTAssert(dress?.images.contains("http://i6.ztat.net/large_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@11.jpg") ?? false)
            XCTAssert(dress?.images.contains("http://i5.ztat.net/large_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@10.jpg") ?? false)
            XCTAssert(dress?.images.contains("http://i1.ztat.net/large_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@9.jpg") ?? false)
            XCTAssert(dress?.images.contains("http://i5.ztat.net/large_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@7.jpg") ?? false)
            XCTAssert(dress?.images.contains("http://i4.ztat.net/large_hd/ZA/82/1C/04/IB/11/ZA821C04I-B11@8.jpg") ?? false)
            
            dressExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedEndpoint[.get], "dresses/ZA821C04I-B11")
        
        waitForExpectations(timeout: 1) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testDressDetailsFailedLoad() {
        let failedDressDetailsExpecation = expectation(description: "failed_dress_details")
        
        failingService.get(dress: "ZA821C04I-B11") { (dress, error) in
            XCTAssertNil(dress)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            
            failedDressDetailsExpecation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedEndpoint[.get], "dresses/ZA821C04I-B11")
        
        waitForExpectations(timeout: 1) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
}
