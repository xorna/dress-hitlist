//
//  DefaultHitListServiceTests.swift
//  Hit ListTests
//
//  Created by Andre Gustavo on 26/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import XCTest
@testable import Hit_List

class DefaultHitListServiceTests: XCTestCase {
    
    var client: ClientDouble!
    var service: DefaultHitListService!
    
    var failingClient: ClientDouble!
    var failingService: DefaultHitListService!
    
    override func setUp() {
        super.setUp()
        let fileMap = [
            "get_hitlist" : "hitList",
            "post_hitlist/lines" : "newLine",
            "delete_hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787" : "emptyList",
            "get_hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787" : "newLine",
            "put_hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787" : "updatedLine"
        ]
        client = ClientDouble(fileMap: fileMap)
        service = DefaultHitListService(client: client)
        
        let fakeFileMap = [
            "get_hitlist" : "notFound",
            "post_hitlist/lines" : "notFound",
            "delete_hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787" : "notFound",
            "get_hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787" : "notFound",
            "put_hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787" : "notFound"
        ]
        
        failingClient = ClientDouble(fileMap: fakeFileMap)
        failingService = DefaultHitListService(client: failingClient)
    }
    
    override func tearDown() {
        service = nil
        failingService = nil
        client = nil
        failingClient = nil
        super.tearDown()
    }
    
    func testGetHitlist() {
        let getHistListExpectation = expectation(description: "get_hit_list")
        
        service.getHitlist { (list, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(list)
            XCTAssertEqual(list?.rating, 4)
            XCTAssertEqual(list?.lines.count, 1)
            XCTAssertEqual(list?.lines.first?.dressId, "ZA821C04I-B11")
            XCTAssertEqual(list?.lines.first?.id, "146506f6-d97f-45c4-9e0e-19461d2aa787")
            XCTAssertEqual(list?.lines.first?.rating, 4)
            XCTAssertEqual(list?.total, 1)
            
            getHistListExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedEndpoint[.get], "hitlist")
        XCTAssertNil(client.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedGetHitList() {
        let failedGetHistListExpectation = expectation(description: "failed_get_hit_list")
        
        failingService.getHitlist { (list, error) in
            
            XCTAssertNil(list)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            
            failedGetHistListExpectation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedEndpoint[.get], "hitlist")
        XCTAssertNil(failingClient.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testSaveNewLine() {
        let saveNewHitListExpectation = expectation(description: "save_new_line")
        
        let line = Line(id: nil, dressId: "ZA821C04I-B11", rating: 4)
        
        service.save(line: line) { (line, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(line)
            XCTAssertEqual(line?.id, "146506f6-d97f-45c4-9e0e-19461d2aa787")
            XCTAssertEqual(line?.dressId, "ZA821C04I-B11")
            XCTAssertEqual(line?.rating, 4)
            
            saveNewHitListExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedEndpoint[.post], "hitlist/lines")
        if let payload = client.receivedParams[.post] as? Line {
            XCTAssertEqual(payload, line)
        } else {
            XCTAssertNotNil(nil)
        }
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedSaveNewLine() {
        let failedSaveNewHitListExpectation = expectation(description: "failed_save_new_line")
        
        let line = Line(id: nil, dressId: "ZA821C04I-B11", rating: 4)
        
        failingService.save(line: line) { (line, error) in
            
            XCTAssertNil(line)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            
            failedSaveNewHitListExpectation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedEndpoint[.post], "hitlist/lines")
        if let payload = failingClient.receivedParams[.post] as? Line {
            XCTAssertEqual(payload, line)
        } else {
            XCTAssertNotNil(nil)
        }
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testGetLine() {
        let getLineExpectation = expectation(description: "get_line")
        
        service.get(line: "146506f6-d97f-45c4-9e0e-19461d2aa787") { (line, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(line)
            XCTAssertEqual(line?.id, "146506f6-d97f-45c4-9e0e-19461d2aa787")
            XCTAssertEqual(line?.dressId, "ZA821C04I-B11")
            XCTAssertEqual(line?.rating, 4)
            
            getLineExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedEndpoint[.get], "hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787")
        XCTAssertNil(client.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedGetLine() {
        let getLineExpectation = expectation(description: "failed_get_line")
        
        failingService.get(line: "146506f6-d97f-45c4-9e0e-19461d2aa787") { (line, error) in
            
            XCTAssertNil(line)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            
            getLineExpectation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedEndpoint[.get], "hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787")
        XCTAssertNil(failingClient.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testUpdateLine() {
        let updateLineExpectation = expectation(description: "update_line")
        
        let line = Line(id: "146506f6-d97f-45c4-9e0e-19461d2aa787", dressId: "ZA821C04I-B11", rating: 5)
        
        service.update(line: line) { (line, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(line)
            XCTAssertEqual(line?.id, "146506f6-d97f-45c4-9e0e-19461d2aa787")
            XCTAssertEqual(line?.dressId, "ZA821C04I-B11")
            XCTAssertEqual(line?.rating, 5)
            
            updateLineExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedParams[.put] as? Line, line)
        XCTAssertEqual(client.receivedEndpoint[.put], "hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787")
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedUpdateLineEmptyId() {
        let failedUpdateLineExpectation = expectation(description: "failed_update_line_empty_id")
        
        let line = Line(id: "146506f6-d97f-45c4-9e0e-19461d2aa787", dressId: "ZA821C04I-B11", rating: 4)
        
        failingService.update(line: line) { (line, error) in
            XCTAssertNil(line)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            failedUpdateLineExpectation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedParams[.put] as? Line, line)
        XCTAssertEqual(failingClient.receivedEndpoint[.put], "hitlist/lines/146506f6-d97f-45c4-9e0e-19461d2aa787")
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedUpdateLineNotFound() {
        let failedUpdateLineExpectation = expectation(description: "failed_update_line_not_found")
        
        let line = Line(id: nil, dressId: "ZA821C04I-B11", rating: 4)
        
        service.update(line: line) { (line, error) in
            XCTAssertNil(line)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Id not found")
            failedUpdateLineExpectation.fulfill()
        }
        
        XCTAssertNil(client.receivedParams[.put])
        XCTAssertNil(client.receivedEndpoint[.put])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
}
