//
//  DefaultSimilarServiceTests.swift
//  Hit ListTests
//
//  Created by Andre Gustavo on 26/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import XCTest
@testable import Hit_List

class DefaultSimilarServiceTests: XCTestCase {
    
    var client: ClientDouble!
    var service: DefaultSimilarService!
    
    var failingClient: ClientDouble!
    var failingService: DefaultSimilarService!
    
    override func setUp() {
        super.setUp()
        let fileMap = [
            "get_similar/appearance/ZA821C04I-B11" : "similarAppearance",
            "get_similar/color/ZA821C04I-B11" : "similarColor",
            "get_similar/shape/ZA821C04I-B11" : "similarShape",
            "get_similar/texture/ZA821C04I-B11" : "similarTexture"
        ]
        client = ClientDouble(fileMap: fileMap)
        service = DefaultSimilarService(client: client)
        
        let fakeFileMap = [
            "get_similar/appearance/ZA821C04I-B11" : "notFound",
            "get_similar/color/ZA821C04I-B11" : "notFound",
            "get_similar/shape/ZA821C04I-B11" : "notFound",
            "get_similar/texture/ZA821C04I-B11" : "notFound"
        ]
        
        failingClient = ClientDouble(fileMap: fakeFileMap)
        failingService = DefaultSimilarService(client: failingClient)
    }
    
    override func tearDown() {
        service = nil
        failingService = nil
        client = nil
        failingClient = nil
        super.tearDown()
    }
    
    func testFindSimilarAppearance() {
        let similarAppearanceExpectation = expectation(description: "find_similar_appearance_dresses")
        
        service.find(id: "ZA821C04I-B11", by: .appearance) { (list, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(list)
            XCTAssertEqual(list?.pageCount, 1)
            XCTAssertEqual(list?.items.count, 5)
            XCTAssertEqual(list?.items.first?.brandName, "edc by Esprit")
            XCTAssertEqual(list?.items.first?.id, "ED121C06I-B11")
            XCTAssertEqual(list?.items.first?.name, "Jumper dress - taupe")
            XCTAssertEqual(list?.items.first?.price, 28.99)
            XCTAssertEqual(list?.items.first?.thumbnails.count, 2)
            XCTAssertEqual(list?.items.first?.thumbnails.first, "http://i5.ztat.net/catalog_hd/ED/12/1C/06/IB/11/ED121C06I-B11@10.jpg")
            XCTAssertEqual(list?.items.first?.thumbnails.last, "http://i5.ztat.net/catalog_hd/ED/12/1C/06/IB/11/ED121C06I-B11@9.jpg")
            
            similarAppearanceExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedEndpoint[.get], "similar/appearance/ZA821C04I-B11")
        XCTAssertNil(client.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedFindSimilarAppearance() {
        let failedSimilarAppearanceExpectation = expectation(description: "failed_find_similar_appearance_dresses")
        
        failingService.find(id: "ZA821C04I-B11", by: .appearance) { (list, error) in
            
            XCTAssertNil(list)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            
            failedSimilarAppearanceExpectation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedEndpoint[.get], "similar/appearance/ZA821C04I-B11")
        XCTAssertNil(failingClient.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFindSimilarColor() {
        let similarColorExpectation = expectation(description: "find_similar_color_dresses")
        
        service.find(id: "ZA821C04I-B11", by: .color) { (list, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(list)
            XCTAssertEqual(list?.pageCount, 1)
            XCTAssertEqual(list?.items.count, 5)
            XCTAssertEqual(list?.items.first?.brandName, "Wood Wood")
            XCTAssertEqual(list?.items.first?.id, "WO421C00C-C11")
            XCTAssertEqual(list?.items.first?.name, "ROSA  - Jumper dress - light grey melange")
            XCTAssertEqual(list?.items.first?.price, 109.99)
            XCTAssertEqual(list?.items.first?.thumbnails.count, 2)
            XCTAssertEqual(list?.items.first?.thumbnails.first, "http://i6.ztat.net/catalog_hd/WO/42/1C/00/CC/11/WO421C00C-C11@14.jpg")
            XCTAssertEqual(list?.items.first?.thumbnails.last, "http://i5.ztat.net/catalog_hd/WO/42/1C/00/CC/11/WO421C00C-C11@13.jpg")
            
            similarColorExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedEndpoint[.get], "similar/color/ZA821C04I-B11")
        XCTAssertNil(client.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedFindSimilarColor() {
        let failedSimilarColorExpectation = expectation(description: "failed_find_similar_color_dresses")
        
        failingService.find(id: "ZA821C04I-B11", by: .color) { (list, error) in
            
            XCTAssertNil(list)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            
            failedSimilarColorExpectation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedEndpoint[.get], "similar/color/ZA821C04I-B11")
        XCTAssertNil(failingClient.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFindSimilarShape() {
        let similarShapeExpectation = expectation(description: "find_similar_shape_dresses")
        
        service.find(id: "ZA821C04I-B11", by: .shape) { (list, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(list)
            XCTAssertEqual(list?.pageCount, 1)
            XCTAssertEqual(list?.items.count, 5)
            XCTAssertEqual(list?.items.first?.brandName, "Superdry")
            XCTAssertEqual(list?.items.first?.id, "SU221C057-Q11")
            XCTAssertEqual(list?.items.first?.name, "AUGUSTA - Jersey dress - black")
            XCTAssertEqual(list?.items.first?.price, 31.49)
            XCTAssertEqual(list?.items.first?.thumbnails.count, 2)
            XCTAssertEqual(list?.items.first?.thumbnails.first, "http://i2.ztat.net/catalog_hd/SU/22/1C/05/7Q/11/SU221C057-Q11@12.jpg")
            XCTAssertEqual(list?.items.first?.thumbnails.last, "http://i3.ztat.net/catalog_hd/SU/22/1C/05/7Q/11/SU221C057-Q11@11.jpg")
            
            similarShapeExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedEndpoint[.get], "similar/shape/ZA821C04I-B11")
        XCTAssertNil(client.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedFindSimilarShape() {
        let failedSimilarShapeExpectation = expectation(description: "failed_find_similar_shape_dresses")
        
        failingService.find(id: "ZA821C04I-B11", by: .shape) { (list, error) in
            
            XCTAssertNil(list)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            
            failedSimilarShapeExpectation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedEndpoint[.get], "similar/shape/ZA821C04I-B11")
        XCTAssertNil(failingClient.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFindSimilarTexture() {
        let similarTextureExpectation = expectation(description: "find_similar_texture_dresses")
        
        service.find(id: "ZA821C04I-B11", by: .texture) { (list, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(list)
            XCTAssertEqual(list?.pageCount, 1)
            XCTAssertEqual(list?.items.count, 5)
            XCTAssertEqual(list?.items.first?.brandName, "Missguided")
            XCTAssertEqual(list?.items.first?.id, "M0Q21C085-C11")
            XCTAssertEqual(list?.items.first?.name, "Jersey dress - grey marl")
            XCTAssertEqual(list?.items.first?.price, 24.99)
            XCTAssertEqual(list?.items.first?.thumbnails.count, 2)
            XCTAssertEqual(list?.items.first?.thumbnails.first, "http://i5.ztat.net/catalog_hd/M0/Q2/1C/08/5C/11/M0Q21C085-C11@2.3.jpg")
            XCTAssertEqual(list?.items.first?.thumbnails.last, "http://i2.ztat.net/catalog_hd/M0/Q2/1C/08/5C/11/M0Q21C085-C11@11.3.jpg")
            
            similarTextureExpectation.fulfill()
        }
        
        XCTAssertEqual(client.receivedEndpoint[.get], "similar/texture/ZA821C04I-B11")
        XCTAssertNil(client.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    func testFailedFindSimilarTexture() {
        let failedSimilarTextureExpectation = expectation(description: "failed_find_similar_texture_dresses")
        
        failingService.find(id: "ZA821C04I-B11", by: .texture) { (list, error) in
            
            XCTAssertNil(list)
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.localizedDescription, "Not found")
            
            failedSimilarTextureExpectation.fulfill()
        }
        
        XCTAssertEqual(failingClient.receivedEndpoint[.get], "similar/texture/ZA821C04I-B11")
        XCTAssertNil(failingClient.receivedParams[.get])
        
        waitForExpectations(timeout: 1.0) { (error) in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
    }
}
