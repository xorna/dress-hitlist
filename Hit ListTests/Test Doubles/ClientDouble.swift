//
//  ClientDouble.swift
//  Hit ListTests
//
//  Created by Andre Gustavo on 26/05/18.
//  Copyright © 2018 Andre Gustavo. All rights reserved.
//

import Foundation
@testable import Hit_List

extension FTError {
    static let notFound = FTError(localizedDescription: "File not found")
}

enum ClientMethod {
    case get
    case post
    case delete
    case put
}

class ClientDouble {
    
    private let fileMap: [String: String]
    
    var receivedParams = [ClientMethod:Any]()
    var receivedEndpoint = [ClientMethod:String]()
    
    required init(fileMap: [String: String]) {
        self.fileMap = fileMap
    }
    
    private func loadFile<T: Codable>(key: String) throws -> T {
        guard let fileName = fileMap[key] else {
            throw FTError.notFound
        }
        
        guard let filePath = Bundle(for: ClientDouble.self).path(forResource: fileName, ofType: ".json") else {
            throw FTError.notFound
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: filePath))
            do {
                return try JSONDecoder().decode(T.self, from: data)
            } catch {
                let error = try JSONDecoder().decode(FTError.self, from: data)
                throw error
            }
        } catch {
            throw error
        }
    }
}

extension ClientDouble: Client {
    func get<T>(endpoint: String, params: [String : Any]?, completion: @escaping (T?, FTError?) -> Void) where T : Codable {
        receivedEndpoint[.get] = endpoint
        
        receivedParams.removeValue(forKey: .get)
        if let params = params {
            receivedParams[.get] = params
        }
        
        do {
            let result: T = try loadFile(key: "get_\(endpoint)")
            completion(result, nil)
        } catch let e as FTError {
            completion(nil, e)
        } catch {
            completion(nil, FTError(localizedDescription: error.localizedDescription))
        }
    }
    
    func post<T, B>(endpoint: String, body: B?, completion: @escaping (T?, FTError?) -> Void) where T : Codable, B : Codable {
        receivedEndpoint[.post] = endpoint
        
        receivedParams.removeValue(forKey: .post)
        if let body = body {
            receivedParams[.post] = body
        }
        
        do {
            let result: T = try loadFile(key: "post_\(endpoint)")
            completion(result, nil)
        } catch let e as FTError {
            completion(nil, e)
        } catch {
            completion(nil, FTError(localizedDescription: error.localizedDescription))
        }
    }
    
    func put<T, B>(endpoint: String, body: B?, completion: @escaping (T?, FTError?) -> Void) where T : Codable, B : Codable {
        receivedEndpoint[.put] = endpoint
        
        receivedParams.removeValue(forKey: .put)
        if let body = body {
            receivedParams[.put] = body
        }
        
        do {
            let result: T = try loadFile(key: "put_\(endpoint)")
            completion(result, nil)
        } catch let e as FTError {
            completion(nil, e)
        } catch {
            completion(nil, FTError(localizedDescription: error.localizedDescription))
        }
    }
    
    func delete(endpoint: String, params: [String : Any]?, completion: @escaping (FTError?) -> Void) {
        
        receivedEndpoint[.delete] = endpoint
        
        receivedParams.removeValue(forKey: .delete)
        if let params = params {
            receivedParams[.delete] = params
        }
        
        do {
            let error: FTError = try loadFile(key: "delete_\(endpoint)")
            completion(error)
        } catch is FTError {
            completion(nil)
        } catch {
            completion(FTError(localizedDescription: error.localizedDescription))
        }
    }
}
