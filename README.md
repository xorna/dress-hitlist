# Dress hitlist

In the fashion industry it's common to manually select a set of products that fit the style of your store. This assessment is about creating a tool that brings that process online.

You are creating this solution for Sophie and Ans, who run a specialized store that sells dresses. Sophie is the owner, but Ans primarily works on the selection process. When Ans has decided on a short list of dresses (hitlist), Sophie will have a final look at it, before she orders the new styles.

To achieve this, you need to build a UI on top of our dress-service where Ans can browse the collection of dresses, and save the ones she likes to the hitlist, including a rating. Sophie will later on review the hitlist and order.

## Assignment

Our back-end is packaged in a Docker container. You'll have to run the service so you can use it with your front-end.

### Running the container
```
docker run -p 8080:8080 fashionablecontainers/dress-hitlist-backend:global-session
```

The container is publicly hosted on Dockerhub and should run out of the box. **Please make sure that the container has at least 2gb of memory available.** Starting the container can take a while before it starts to accept requests.

### API documentation

After running you can visit the API documentation by going to `http://localhost:8080/ui/`. On this page, you can also try out the different endpoints.

If you are running Docker on macOS, the API is exposed on the Docker virtual machine, so you will need to connect to the docker-machine IP.

### Specification

Required features:

- Browse dresses
- See the details of a dress
- See your hitlist
- Add a dress to the hitlist with a rating

In order to make for a nicer browsing experience, you can consider using the APIs that help you select / find similar dresses based on color, texture, shape and appearance (see the `/similar/*` API endpoints for this). Also, you may want to add more functionality for editing the hitlist or other things that are not listed in the requirements, if you think it's an improvement.

## Running the app

#### Cloning the project
```
git clone git@bitbucket.org:xorna/dress-hitlist.git
```

After cloning the project, it's necessary to install all dependencies using cocoapods (see `http://cocoapods.org`):

```
pod install
```

After all set, instead of opening the project file, open the workspace file `Hit List.xcworkspace`.
